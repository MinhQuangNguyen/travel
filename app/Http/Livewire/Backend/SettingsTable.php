<?php

namespace App\Http\Livewire\Backend;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class SettingsTable.
 */
class SettingsTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'name';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Setting::query();
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->sortable()
                ->searchable(function ($builder, $term) {
                    return $builder->where('name', '%'.$term.'%');
                })
                ->format(function (Setting $model) {
                    return view('backend.setting.includes.name', ['setting' => $model]);
                }),
            Column::make(__('Value'), 'value')
                ->sortable()
                ->searchable(function ($builder, $term) {
                    return $builder->orWhere('value', '%'.$term.'%');
                })
                ->format(function (Setting $model) {
                    return view('backend.setting.includes.value', ['setting' => $model]);
                }),
            Column::make(__('CreatedAt'), 'created_at')
                ->sortable()
                ->format(function (Setting $model) {
                    return date('d/m/Y', strtotime($model->created_at));
                }),
            Column::make(__('Actions'))
                ->format(function (Setting $model) {
                    return view('backend.setting.includes.actions', ['setting' => $model]);
                }),
        ];
    }
}
