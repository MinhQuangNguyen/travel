<?php

namespace App\Http\Livewire\Backend;

use App\Models\Menu;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class MenusTable.
 */
class MenusTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'sort';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Menu::query()->orderBy('sort', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('name', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('menus.*')->join('menu_translations as t', function ($query) use($locale) {
                        $query->on('menus.id', '=', 't.menu_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.name', $direction);
                })
                ->format(function (Menu $model) {
                    return view('backend.menu.includes.name', ['menu' => $model]);
                }),
            Column::make(__('Parent'), 'parent_id')
                ->format(function (Menu $model) {
                    $parent = $model::where('id', $model->parent_id)->first();
                    return view('backend.menu.includes.parent', ['parent' => $parent]);
                }),
            Column::make(__('Category'), 'category_id')
                ->format(function (Menu $model) {
                    $category = Category::find($model->category_id);
                    return view('backend.menu.includes.category', ['category' => $category]);
                }),
            Column::make(__('Sort'), 'sort')
                ->format(function (Menu $model) {
                    return view('backend.menu.includes.sort', ['menu' => $model]);
                }),
            Column::make(__('Link'), 'link')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('link', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('menus.*')->join('menu_translations as t', function ($query) use($locale) {
                        $query->on('menus.id', '=', 't.menu_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.link', $direction);
                })
                ->format(function (Menu $model) {
                    return view('backend.menu.includes.link', ['menu' => $model]);
                }),
            Column::make(__('CreatedAt'), 'created_at')
                ->sortable()
                ->format(function (Menu $model) {
                    return date('d/m/Y', strtotime($model->created_at));
                }),
            Column::make(__('Actions'))
                ->format(function (Menu $model) {
                    return view('backend.menu.includes.actions', ['menu' => $model]);
                }),
        ];
    }
}
