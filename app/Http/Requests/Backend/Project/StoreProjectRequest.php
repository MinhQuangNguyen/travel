<?php

namespace App\Http\Requests\Backend\Project;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreProjectRequest.
 */
class StoreProjectRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['required', 'image', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'category_id' => ['required', 'numeric'],
            'name' => ['required', 'max:255'],
            'name_en' => ['required', 'max:255'],
            'address' => ['required', 'max:255'],
            'address_en' => ['required', 'max:255'],
            'description' => ['max:255'],
            'description_en' => ['max:255'],
            'link' => ['required', 'max:255'],
            'link_en' => ['required', 'max:255']
        ];
    }
}
