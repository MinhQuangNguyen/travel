<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\PostService;
use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Class NewsController.
 */
class DetailTourController extends Controller
{
    /**
     * NewsController constructor.
     *
     * @param  PostService  $postService
     * @param  CategoryService  $categoryService
     */
    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $slug = null)
    {
        $categories = $this->categoryService->getNewCategories();
        $category_id = null;
        $category = null;

        if($slug) {
            $category = $this->categoryService->getCategoryBySlug($slug);
            $category_id = $category->id;
        }
        $slides = $this->postService->getPostHighLight($category_id);
        $keyword = $request->get('keyword');
        $posts = $this->postService->getPostByCategory($category_id, $keyword);
        $libraries = $this->categoryService->getPostByCategoryType(2);
        $res = [];
        foreach($slides as $slide) {
            $title = $slide->category->type === Category::TYPE_LIBRARY ? $slide->type === 0 ? 'Hình ảnh' : 'Video' : $slide->category->name;
            array_push($res, [
                'title' => $title,
                'content' => $slide->title,
                'image' => $slide->banner,
            ]);
        }
        return view('frontend.detailTour.index', [
            'categories' => $categories,
            'slides' => json_decode(json_encode($res)),
            'posts' => $posts,
            'libraries' => $libraries
        ]);
    }


}
