<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\DestinationService;
use Illuminate\Http\Request;

/**
 * Class DestinationController.
 */
class DestinationController extends Controller
{
    /**
     * @var DestinationService
     */
    protected $DestinationService;

    /**
     * DestinationController constructor.
     *
     * @param  DestinationService  $DestinationService
     */
    public function __construct(DestinationService $DestinationService)
    {
        $this->DestinationService = $DestinationService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.destination.index');
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    // public function store(Request $request)
    // {
    //     $data = $request->only('name', 'phone', 'company','email', 'content');
    //     $this->DestinationService->store($data);
    //     $locale = session()->get('locale') ?? env('APP_LANGUAGE');
    //     $pageName = $locale === "vi" ? 'lien-he' : 'contact';
    //     return redirect("/lien-he#contact-scroll")->with('success', 'Cảm ơn bạn đã liên hệ, tôi sẽ cố gắng phản hồi lại với bạn!');
    // }
}
