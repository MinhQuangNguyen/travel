<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Contact\EditContactRequest;
use App\Http\Requests\Backend\Contact\UpdateContactRequest;
use App\Http\Requests\Backend\Contact\DeleteContactRequest;
use App\Models\Contact;
use App\Services\ContactService;
use App\Http\Controllers\Controller;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{
    /**
     * @var ContactService
     */
    protected $contactService;

    /**
     * ContactController constructor.
     *
     * @param  ContactService  $contactService
     */
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.contact.index');
    }

    /**
     * @param  EditContactRequest  $request
     * @param  Contact  $contact
     *
     * @return mixed
     */
    public function detail(EditContactRequest $request, Contact $contact)
    {
        return view('backend.contact.detail')->withContact($contact);
    }

    /**
     * @param  UpdateContactRequest  $request
     * @param  Contact  $contact
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateContactRequest $request, Contact $contact)
    {
        $data = $request->validated();
        $this->contactService->update($contact, $data);

        return redirect()->route('admin.contact.index')->withFlashSuccess(__('The contact was successfully updated.'));
    }

    /**
     * @param  DeleteContactRequest  $request
     * @param  Contact  $contact
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteContactRequest $request, Contact $contact)
    {
        $this->contactService->destroy($contact);

        return redirect()->route('admin.contact.index')->withFlashSuccess(__('The contact was successfully deleted.'));
    }
}
