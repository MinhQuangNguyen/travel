<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Services\CategoryService;
use App\Services\SlideService;
use App\Http\Controllers\Controller;

/**
 * Class BannerController.
 */
class BannerController extends Controller
{
    /**
     * @var CategoryService
     * @var SlideService
     */
    protected $categoryService;
    protected $slideService;

    /**
     * BannerController constructor.
     *
     * @param  CategoryService  $categoryService
     */
    public function __construct(CategoryService $categoryService, SlideService $slideService)
    {
        $this->categoryService = $categoryService;
        $this->slideService = $slideService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.banner.index');
    }

    /**
     * @param  Request  $request
     * @param  Category  $category
     *
     * @return mixed
     */
    public function edit(Request $request, Category $banner)
    {
        $slides = $this->slideService->getSlides($banner->id);
        $data = [];
        foreach($slides as $key => $slide) {
            $data[$key]['title'] = $slide->translate('vi')->title;
            $data[$key]['content'] = $slide->translate('vi')->content;
            $data[$key]['link'] = $slide->translate('vi')->link;
            $data[$key]['title_en'] = $slide->translate('en')->title;
            $data[$key]['content_en'] = $slide->translate('en')->content;
            $data[$key]['link_en'] = $slide->translate('en')->link;
            $data[$key]['image'] = $slide->image;
            $data[$key]['sort'] = $slide->sort;
        }
        return view('backend.banner.edit')
            ->withSlides($data)
            ->withBanner($banner);
    }

    /**
     * @param  Request  $request
     * @param  Category  $banner
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(Request $request, Category $banner)
    {
        $data = $request->only('name', 'description', 'key', 'active', 'slides');
        $dataC = [
            "name" => $data['name'],
            "description" => $data['description'],
            "key" => $data['key'],
            "active" => $data['active'],
        ];

        $this->categoryService->update($banner, $dataC);

        if($banner->slides()->count()) {
            $banner->slides()->delete();
        }

        foreach ($data['slides'] as $key => $data) {
            $this->slideService->store($data, $banner->id);
        }

        return redirect()->route('admin.banner.index')->withFlashSuccess(__('The banner was successfully updated.'));
    }
}
