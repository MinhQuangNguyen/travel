<?php

namespace App\Services;

use App\Models\Address;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class AddressService.
 */
class AddressService extends BaseService
{
    /**
     * AddressService constructor.
     *
     * @param  Address  $address
     */
    public function __construct(Address $address)
    {
        $this->model = $address;
    }

    /**
     * @return mixed
     */
    public function getAddresses($id = null)
    {
        $query = $this->model::where('active', 1)->orderBy('sort', 'asc');
        return $query->get();
    }
}
