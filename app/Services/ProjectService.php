<?php

namespace App\Services;

use App\Events\Project\ProjectCreated;
use App\Events\Project\ProjectDeleted;
use App\Events\Project\ProjectUpdated;
use App\Models\Project;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class ProjectService.
 */
class ProjectService extends BaseService
{
    /**
     * ProjectService constructor.
     *
     * @param  Project  $project
     */
    public function __construct(Project $project)
    {
        $this->model = $project;
    }

    /**
     * @param  array  $data
     *
     * @return Project
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Project
    {
        DB::beginTransaction();
        try {
            $dataT = [
                'image' => $data['image'],
                'banner' => $data['banner'] ?? null,
                'category_id' => $data['category_id'],
                'is_highlight' => $data['is_highlight'] ?? 0,
                'vi' => [
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'description' => $data['description'],
                    'slug' => $data['slug'],
                    'overviews' => [
                        'title' => $data['overviews']['title'],
                        'content' => $data['overviews']['content']
                    ],
                    'positions' => [
                        'title' => $data['positions']['title'],
                        'description_short' => $data['positions']['description_short'],
                        'content' => $data['positions']['content'],
                        'image' => $data['positions']['image'] ?? null
                    ],
                    'utilities' => [
                        'title' => $data['utilities']['title'],
                        'content' => $data['utilities']['content'],
                        'caption' => $data['utilities']['caption'] ?? null,
                        'images' => $data['utilities']['images'] ?? null,
                    ],
                    'images' => [
                        'title' => $data['images']['title'],
                        'content' => $data['images']['content'],
                        'caption' => $data['images']['caption'] ?? null,
                        'images' => $data['images']['images'] ?? null,
                    ]
                ],
                'en' => [
                    'name' => $data['name_en'],
                    'address' => $data['address_en'],
                    'description' => $data['description_en'],
                    'slug' => $data['slug_en'],
                    'overviews' => [
                        'title' => $data['overviews']['title_en'],
                        'content' => $data['overviews']['content_en']
                    ],
                    'positions' => [
                        'title' => $data['positions']['title_en'],
                        'description_short' => $data['positions']['description_short_en'],
                        'content' => $data['positions']['content_en'],
                        'image' => $data['positions']['image'] ?? null
                    ],
                    'utilities' => [
                        'title' => $data['utilities']['title_en'],
                        'content' => $data['utilities']['content_en'],
                        'caption' => $data['utilities']['caption_en'] ?? null,
                        'images' => $data['utilities']['images'] ?? null,
                    ],
                    'images' => [
                        'title' => $data['images']['title_en'],
                        'content' => $data['images']['content_en'],
                        'caption' => $data['images']['caption_en'] ?? null,
                        'images' => $data['images']['images'] ?? null
                    ]
                ]
            ];

            $project = $this->model::create($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the project.'));
        }

        event(new ProjectCreated($project));

        DB::commit();

        return $project;
    }

    /**
     * @param  Project  $project
     * @param  array  $data
     *
     * @return Project
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Project $project, array $data = []): Project
    {
        DB::beginTransaction();
        try {
            $locale = session()->get('locale') ?? env('APP_LANGUAGE');
            $lang = $locale == 'vi' ? 'en' : 'vi';
            $dataT = [
                'image' => $data['image'] ?? $project->image,
                'banner' => $data['banner'] ?? $project->banner,
                'category_id' => $data['category_id'],
                'is_highlight' => $data['is_highlight'] ?? 0,
                $locale => [
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'description' => $data['description'],
                    'slug' => $data['slug'],
                    'overviews' => [
                        'title' => $data['overviews']['title'],
                        'content' => $data['overviews']['content']
                    ],
                    'positions' => [
                        'title' => $data['positions']['title'],
                        'description_short' => $data['positions']['description_short'],
                        'content' => $data['positions']['content'],
                        'image' => $data['positions']['image'] ?? $project->positions['image']
                    ],
                    'utilities' => [
                        'title' => $data['utilities']['title'],
                        'content' => $data['utilities']['content'],
                        'caption' => $data['utilities']['caption'] ?? [],
                        'images' => $data['utilities']['images'] ?? [],
                    ],
                    'images' => [
                        'title' => $data['images']['title'],
                        'content' => $data['images']['content'],
                        'caption' => $data['images']['caption'] ?? [],
                        'images' => $data['images']['images'] ?? [],
                    ]
                ],
                $lang => [
                    'utilities' => [
                        'title' => $project->utilities['title'],
                        'content' => $project->utilities['content'],
                        'caption' => isset($data['utilities']['caption']) ? $project->translate($lang)->utilities['caption'] : [],
                        'images' => $data['utilities']['images'] ?? [],
                    ],
                    'images' => [
                        'title' => $project->images['title'],
                        'content' => $project->images['content'],
                        'caption' => isset($data['images']['caption']) ? $project->translate($lang)->images['caption'] : [],
                        'images' => $data['images']['images'] ?? [],
                    ]
                ]
            ];
            $project->update($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the project.'));
        }

        event(new ProjectUpdated($project));

        DB::commit();

        return $project;
    }

    /**
     * @param  Project  $project
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Project $project): bool
    {
        if ($this->deleteById($project->id)) {
            event(new ProjectDeleted($project));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the project.'));
    }

    //Lấy dự án nổi bật
    public function getProjectOutstanding($category_id = null, $limit = 5) {
        $query = $this->model->query();
        if($category_id) $query = $query->where('category_id', $category_id);
        return $query
            ->where('is_highlight', 1)
            ->limit($limit)
            ->get();
    }

    //lấy tất cả dự án theo category
    public function getProjectByCategory($category_id = null, $keyword = null) {
        $query = $this->model->query();
        if($category_id) $query = $query->where('category_id', $category_id);
        if($keyword) $query = $query->where(function($builder) use($keyword){
            $builder->orWhereTranslationLike('name',"%$keyword%");
            $builder->orWhereTranslationLike('description','like', "%$keyword%");
        });
        return $query->paginate(6);
    }

    public function getProjectBySlug($slug) {
        $project = $this->model->query()->whereTranslationLike('slug', $slug)->first();
        return $project;
    }
}
