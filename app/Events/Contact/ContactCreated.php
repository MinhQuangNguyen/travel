<?php

namespace App\Events\Contact;

use App\Models\Contact;
use Illuminate\Queue\SerializesModels;

/**
 * Class ContactCreated.
 */
class ContactCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $contact;

    /**
     * @param $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }
}
