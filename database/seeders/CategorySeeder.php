<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\CategoryTranslation;
use Database\Seeders\Traits\DisableForeignKeys;
use Database\Seeders\Traits\TruncateTable;

class CategorySeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        $this->truncate('categories');
        $this->truncate('category_translations');

        if (app()->environment(['local', 'testing'])) {
            Category::insert([
                [
                    'parent_id' => null,
                    'active' => 1,
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => null,
                    'active' => 1,
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
               ]
            ]);

            CategoryTranslation::insert([
                [
                    'category_id' => 1,
                    'locale' => 'vi',
                    'name' => 'Tin Khang Chi',
                    'description' => null,
                    'slug' => 'tin-khang-chi'
                ],
                [
                    'category_id' => 1,
                    'locale' => 'en',
                    'name' => 'Khang Chi news',
                    'description' => null,
                    'slug' => 'khang-chi-news',
                ],
                [
                    'category_id' => 2,
                    'locale' => 'vi',
                    'name' => 'Tin thị trường',
                    'description' => null,
                    'slug' => 'tin-thi-truong'
                ],
                [
                    'category_id' => 2,
                    'locale' => 'en',
                    'name' => 'Market news',
                    'description' => null,
                    'slug' => 'maket-news'
                ]
            ]);
        }

        $this->enableForeignKeys();
    }
}
