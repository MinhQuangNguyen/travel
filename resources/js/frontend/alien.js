

function partnerSlide() {
    $(".partner-home-slide").slick({
        dots: false,
        infinite: true,
        mobileFirst: true,
        // variableWidth: true,
        speed: 300,
        slidesToShow: 1,
        prevArrow: $('.partner-prev'),
        nextArrow: $('.partner-next'),

        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    })
}


function tourSlide() {
    $(".baner-slide-tour-relate").slick({
        infinite: true,
        mobileFirst: true,
        dots: true,
        centerPadding: '60px',
        speed: 300,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    })
}

function changeTabField() {
    //active lại tab theo url
    const $fieldPage = $(".tab-field-main");
    if ($fieldPage.length) {
        const pathName = window.location.pathname;
        $fieldPage.find(`.nav-tabs .nav-item[data-url='${pathName}']`).tab("show")
    }
    //điều chỉnh lại hover trên menu và slide.
    $(".tab-field-main").off("click").on("click", ".nav-item", function() {
        const newBanner = $(this).data("banner");
        const newName = $(this).data("name");
        const newLink = $(this).data("url");
        const navActive = $('.nav-item.dropdown.active')
        if (newLink) {
            $("#slide-field").find('.img').css('background-image', 'url(' + newBanner + ')')
            $("#slide-field").find('.title').text(newName)
            navActive.find('.active').removeClass('active')
            navActive.find(`[data-href='${newLink}']`).addClass('active')
            window.history.pushState('', '', newLink);
        }
    })
}
// End scroll top

// chart bar
(function(document) {
    var _bars = [].slice.call(document.querySelectorAll('.bar-inner'));
    _bars.map(function(bar, index) {
      setTimeout(function() {
          bar.style.width = bar.dataset.percent;
      }, index * 1000);
      
    });
  })(document)


// toogle
$(document).ready(function () {
    $(this).on("click", ".koh-faq-question", function () {
        $(this).parent().find(".koh-faq-answer").toggle();
        $(this).find(".fa").toggleClass('active');
    });
});

// time
function CountDownTime() {
	
		var endTime = new Date("29 April 2021 6:00 GMT+01:00");			
			endTime = (Date.parse(endTime) / 1000);

			var now = new Date();
			now = (Date.parse(now) / 1000);

			var timeLeft = endTime - now;

			var days = Math.floor(timeLeft / 86400); 
			var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
			var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
			var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
			if (hours < "10") { hours = "0" + hours; }
			if (minutes < "10") { minutes = "0" + minutes; }
			if (seconds < "10") { seconds = "0" + seconds; }

			$("#days").html(days + "");
			$("#hours").html(hours + "");
			$("#minutes").html(minutes + "");
			$("#seconds").html(seconds + "");		
	}
setInterval(function() { CountDownTime(); }, 1000);
// End time


// scroll amiantion mqn
function animateFrom(elem, direction) {
    direction = direction || 1;
    var x = 0,
        y = direction * 100;
    if(elem.classList.contains("gs_reveal_fromLeft")) {
      x = -100;
      y = 0;
    } else if (elem.classList.contains("gs_reveal_fromRight")) {
      x = 100;
      y = 0;
    }else if (elem.classList.contains("gs_reveal_fromTop")) {
        x = 0;
        y = 100;
    }  else if (elem.classList.contains("gs_reveal_fromBottom")) {
        x = 0;
        y = -100;
    }
    elem.style.transform = "translate(" + x + "px, " + y + "px)";
    elem.style.opacity = "0";
    gsap.fromTo(elem, {x: x, y: y, autoAlpha: 0}, {
      duration: 1.25, 
      x: 0,
      y: 0, 
      autoAlpha: 1, 
      ease: "expo", 
      overwrite: "auto"
    });
  }
  
  function hide(elem) {
    gsap.set(elem, {autoAlpha: 0});
  }
  
  document.addEventListener("DOMContentLoaded", function() {
    gsap.registerPlugin(ScrollTrigger);
    
    gsap.utils.toArray(".gs_reveal").forEach(function(elem) {
      hide(elem); // assure that the element is hidden when scrolled into view
      
      ScrollTrigger.create({
        trigger: elem,
        onEnter: function() { animateFrom(elem) }, 
        onEnterBack: function() { animateFrom(elem, -1) },
        onLeave: function() { hide(elem) } // assure that the element is hidden when scrolled into view
      });
    });
  });
  
// scroll amiantion mqn 

$("document").ready(function() {
    partnerSlide();
    changeTabField();
    CountDownTime();
    tourSlide();
    animateFrom();
})

