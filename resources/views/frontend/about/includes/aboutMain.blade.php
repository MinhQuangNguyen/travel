<div class="about-main-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block">
                <div class="img-about-pc">
                    <img src="img/about1.png" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-7">
                <div class="content-about-main">
                    <div class="sub-title">About</div>
                    <div class="title">We Help You Planning You Journey</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                    <div class="text">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-blue" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
