<div class="our-team-about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-title-head">
                    <div class="sub-title">Our Team</div>
                    <div class="title">Meat the team</div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-team-menber-main">
                    <img src="img/user-about1.png" alt="team-menber" class="avata-team-menber">
                    <div class="content-info-team">
                        <div class="title-name">Adam Johnson</div>
                        <div class="text-decs"> CEO, Founder</div>
                    </div>
                    <ul>
                        <li><a href=""><img src="img/icon-fb-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-youtube-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-tw-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-ig-about.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-team-menber-main">
                    <img src="img/user-about2.png" alt="team-menber" class="avata-team-menber">
                    <div class="content-info-team">
                        <div class="title-name">Adam Johnson</div>
                        <div class="text-decs"> CEO, Founder</div>
                    </div>
                    <ul>
                        <li><a href=""><img src="img/icon-fb-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-youtube-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-tw-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-ig-about.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-team-menber-main">
                    <img src="img/user-about3.png" alt="team-menber" class="avata-team-menber">
                    <div class="content-info-team">
                        <div class="title-name">Adam Johnson</div>
                        <div class="text-decs"> CEO, Founder</div>
                    </div>
                   <ul>
                        <li><a href=""><img src="img/icon-fb-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-youtube-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-tw-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-ig-about.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-team-menber-main">
                    <img src="img/user-about4.png" alt="team-menber" class="avata-team-menber">
                    <div class="content-info-team">
                        <div class="title-name">Adam Johnson</div>
                        <div class="text-decs"> CEO, Founder</div>
                    </div>
                   <ul>
                        <li><a href=""><img src="img/icon-fb-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-youtube-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-tw-about.svg" alt=""></a></li>
                        <li><a href=""><img src="img/icon-ig-about.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>