<div class="customer backgrond-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-header-customer">
                    <div class="sub-title">Testimonials</div>
                    <div class="title">Customer Reviews</div>
                </div>
            </div>
            <div class="col-12">
                <div id="carouselCustomer" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselCustomer" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselCustomer" data-slide-to="1"></li>
                        <li data-target="#carouselCustomer" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="item-customer-user">
                                <div class="feedback">
                                    <div class="text"> "Needless to say we are extremely satisfied with the results.
                                        Booking tour was the best investment I ever made. Nice work on your booking
                                        tour. Booking tour impressed."</div>
                                </div>
                                <div class="user-customer">
                                    <div class="avat-customer">
                                        <img src="img/quot3.png" class="quot" alt="quot3">
                                        <img src="img/MQN.png" alt="Minh Quang Nguyen">
                                    </div>
                                    <div class="item-info-feedback">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <div class="name">Minh Quang Nguyen</div>
                                        <div class="loaction">Hanoi, Viet Nam</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="item-customer-user">
                                <div class="feedback">
                                    <div class="text"> "Needless to say we are extremely satisfied with the results.
                                        Booking tour was the best investment I ever made. Nice work on your booking
                                        tour. Booking tour impressed."</div>
                                </div>
                                <div class="user-customer">
                                    <div class="avat-customer">
                                        <img src="img/quot3.png" class="quot" alt="quot3">
                                        <img src="img/MQN.png" alt="Minh Quang Nguyen">
                                    </div>
                                    <div class="item-info-feedback">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <div class="name">Minh Quang Nguyen</div>
                                        <div class="loaction">Hanoi, Viet Nam</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="item-customer-user">
                                <div class="feedback">
                                    <div class="text"> "Needless to say we are extremely satisfied with the results.
                                        Booking tour was the best investment I ever made. Nice work on your booking
                                        tour. Booking tour impressed."</div>
                                </div>
                                <div class="user-customer">
                                    <div class="avat-customer">
                                        <img src="img/quot3.png" class="quot" alt="quot3">
                                        <img src="img/MQN.png" alt="Minh Quang Nguyen">
                                    </div>
                                    <div class="item-info-feedback">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <div class="name">Minh Quang Nguyen</div>
                                        <div class="loaction">Hanoi, Viet Nam</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>