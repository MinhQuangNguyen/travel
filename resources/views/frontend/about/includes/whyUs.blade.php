<div class="why-us-process">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-title-head">
                    <div class="sub-title">Why us</div>
                    <div class="title">We Make All The Process Easy</div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="item-why-us-mqn">
                   <div class="icon-item-why">
                        <img src="img/icon-service-about1.svg" alt="icon">
                   </div>
                    <div class="content-item-why-us">
                        <div class="title-item">Best Travel Agent</div>
                        <div class="text-item">The generated Lorem Ipsum is therefore always free from repetition available</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="item-why-us-mqn">
                    <div class="icon-item-why">
                        <img src="img/icon-service-about2.svg" alt="icon">
                   </div>
                    <div class="content-item-why-us">
                        <div class="title-item">Trust & Safety</div>
                        <div class="text-item">The generated Lorem Ipsum is therefore always free from repetition available</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="item-why-us-mqn">
                    <div class="icon-item-why">
                        <img src="img/icon-service-about3.svg" alt="icon">
                   </div>
                    <div class="content-item-why-us">
                        <div class="title-item">Best Price Guarantee</div>
                        <div class="text-item">The generated Lorem Ipsum is therefore always free from repetition available</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="item-why-us-mqn">
                    <div class="icon-item-why">
                        <img src="img/icon-service-about4.svg" alt="icon">
                   </div>
                    <div class="content-item-why-us">
                        <div class="title-item">Beautiful Places</div>
                        <div class="text-item">The generated Lorem Ipsum is therefore always free from repetition available</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="item-why-us-mqn">
                    <div class="icon-item-why">
                        <img src="img/icon-service-about5.svg" alt="icon">
                   </div>
                    <div class="content-item-why-us">
                        <div class="title-item">Passionate Travel</div>
                        <div class="text-item">The generated Lorem Ipsum is therefore always free from repetition available</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="item-why-us-mqn">
                    <div class="icon-item-why">
                        <img src="img/icon-service-about6.svg" alt="icon">
                   </div>
                    <div class="content-item-why-us">
                        <div class="title-item">Fast Booking</div>
                        <div class="text-item">The generated Lorem Ipsum is therefore always free from repetition available</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
