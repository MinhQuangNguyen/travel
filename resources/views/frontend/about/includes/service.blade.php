<div class="service-main-why-mqn">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="item-servie-main-mqn">
                    <img src="img/icon-service-about7.svg" alt="">
                    <div class="number-text">154</div>
                    <div class="text">Destinations</div>
                </div>
            </div>

            <div class="col-12 col-lg-3">
                <div class="item-servie-main-mqn">
                    <img src="img/icon-service-about8.svg" alt="">
                    <div class="number-text">2,165</div>
                    <div class="text">Amazing Tours</div>
                </div>
            </div>

            <div class="col-12 col-lg-3">
                <div class="item-servie-main-mqn">
                    <img src="img/icon-service-about9.svg" alt="">
                    <div class="number-text">98</div>
                    <div class="text">Tour types</div>
                </div>
            </div>

            <div class="col-12 col-lg-3">
                <div class="item-servie-main-mqn">
                    <img src="img/icon-service-about10.svg" alt="">
                    <div class="number-text">117,295</div>
                    <div class="text">Happy customer</div>
                </div>
            </div>
        </div>
    </div>
</div>
