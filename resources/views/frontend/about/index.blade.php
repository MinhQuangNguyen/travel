@extends('frontend.layouts.alien')

@section('title', __('Giới thiệu'))

@section('content')
@include('frontend.about.includes.headerSubPage')
@include('frontend.about.includes.aboutMain')
@include('frontend.about.includes.whyUs')
@include('frontend.about.includes.service')
@include('frontend.about.includes.ourTeam')
@include('frontend.about.includes.customer')
@endsection
