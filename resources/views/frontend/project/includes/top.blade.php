<div class="poputour poputour-list">
    <div class="container-fluid">
        <div class="item-total-sort-mqn">
            <div class="row">
                <div class="col-4 col-lg-6">
                    <div class="total-popup-tour">20 <span>Tours</span></div>
                </div>
                <div class="col-8 col-lg-6">
                    <div class="item-sort-tour-mqn">
                        <div class="item-sort">
                            <div class="text-sort">Sort by</div>
                            <img src="/img/sort.svg" alt="">
                        </div>
                        <div class="dropdown">
                        <select>
                            <option selected>Title</option>
                            <option>Rating</option>
                            <option>Price</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('frontend.project.includes.listTour')
    </div>
</div>


<div class="pagination-mqn">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <ul class="pagination">
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">NEXT <img src="/img/icon-arrow-right.svg" alt="icon-right"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


