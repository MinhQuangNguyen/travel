<div class="item-list-course-main-mqn">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-12 col-lg-6">
                <div class="title-list-blog">
                    <div class="title">Khóa học</div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="list-menu-tags">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#uiux">UI/UX</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#logo">Logo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#banner">banner</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="">
                            <div class="item-courser-avata" style="background-image: url('/img/blog1.svg')">
                                <div class="item-timeline-courser">
                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 24h 43m</span>
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">Học UI/UX website.</div>
                                <div class="text">Hướng dẫn sử dụng phần mềm thiết kế ứng dụng web/App hiện nay.</div>
                                <div class="by">Tác giả : Minh Quang Nguyen</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">2,808 View</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="">
                            <div class="item-courser-avata" style="background-image: url('/img/blog2.svg')">
                                <div class="item-timeline-courser">
                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 24h 43m</span>
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">Học UI/UX website.</div>
                                <div class="text">Hướng dẫn sử dụng phần mềm thiết kế ứng dụng web/App hiện nay.</div>
                                <div class="by">Tác giả : Minh Quang Nguyen</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">2,808 View</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="">
                            <div class="item-courser-avata" style="background-image: url('/img/blog3.svg')">
                                <div class="item-timeline-courser">
                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 24h 43m</span>
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">Học UI/UX website.</div>
                                <div class="text">Hướng dẫn sử dụng phần mềm thiết kế ứng dụng web/App hiện nay.</div>
                                <div class="by">Tác giả : Minh Quang Nguyen</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">2,808 View</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="">
                            <div class="item-courser-avata" style="background-image: url('/img/blog4.svg')">
                                <div class="item-timeline-courser">
                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 24h 43m</span>
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">Học UI/UX website.</div>
                                <div class="text">Hướng dẫn sử dụng phần mềm thiết kế ứng dụng web/App hiện nay.</div>
                                <div class="by">Tác giả : Minh Quang Nguyen</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">2,808 View</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="">
                            <div class="item-courser-avata" style="background-image: url('/img/blog6.svg')">
                                <div class="item-timeline-courser">
                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 24h 43m</span>
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">Học UI/UX website.</div>
                                <div class="text">Hướng dẫn sử dụng phần mềm thiết kế ứng dụng web/App hiện nay.</div>
                                <div class="by">Tác giả : Minh Quang Nguyen</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">2,808 View</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="">
                            <div class="item-courser-avata" style="background-image: url('/img/blog6.svg')">
                                <div class="item-timeline-courser">
                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 24h 43m</span>
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">Học UI/UX website.</div>
                                <div class="text">Hướng dẫn sử dụng phần mềm thiết kế ứng dụng web/App hiện nay.</div>
                                <div class="by">Tác giả : Minh Quang Nguyen</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">2,808 View</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="banner-customer-recrument">
    <div class="container d-block d-lg-none slide-recrument">
        <div class="row">
            <div class="col-12">
                <div class="pagination-main pt-pagination-mqn">
                    <div class="row justify-content-center">
                        <div class="btn-kc btn-back partner-prev pb-pagination">
                            <div class="btn-content"></div>
                        </div>
                        <div class="item-number-pagination pb-pagination">
                            <span class="currentIndex">01</span>
                            <span>/</span>
                            <span class="total">03</span>
                        </div>
                        <div class="btn-kc btn-next partner-next pb-pagination">
                            <div class="btn-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container d-none d-lg-block slide-recrument">
        <div class="row">
            <!-- <div class="col-2 col-xl-2 d-lg-none d-xl-block"></div> -->
            <div class="col-12 col-lg-12">
                <div class="pagination-main pt-pagination-mqn">
                    <div class="row justify-content-center">
                        <div class="btn-kc btn-back partner-prev pb-pagination">
                            <div class="btn-content"></div>
                        </div>
                        <div class="item-number-pagination pb-pagination">
                            <span class="currentIndex">01</span>
                            <span>/</span>
                            <span class="total">03</span>
                        </div>
                        <div class="btn-kc btn-next partner-next pb-pagination">
                            <div class="btn-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
