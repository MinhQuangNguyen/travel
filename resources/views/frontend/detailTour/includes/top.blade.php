<div class="content-detail-tour">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="item-title-detail-tour">
                    <div class="title">7 Days in Costa Rica – Classic (Self-Drive)</div>
                    <div class="decs-map-location">
                        <img src="/img/map-icon.png" alt="icon">
                        <div class="location">Bryce Canyon National Park, USA</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="media-tour">
                    <button>
                        <img src="/img/camera.svg" alt="icon">
                        <span>Gallery</span>
                    </button>
                    <button>
                        <img src="/img/video.svg" alt="icon">
                        <span>video</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="list-img-tour-head">
    <div class="slide-tour-perfect">
        <div class="partner-home-slide-group">
            <div class="partner-home-slide">
                <div class="item">
                    <div class="item-tour-perfect">
                        <img src="img/tour1.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tour-perfect">
                        <img src="img/tour7.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tour-perfect">
                        <img src="img/tour8.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tour-perfect">
                        <img src="img/tour9.png" alt="">
                    </div>
                </div>
            </div>

            <div class="btn-kc btn-back partner-prev">
                <div class="btn-content"></div>
            </div>
            <div class="btn-kc btn-next partner-next">
                <div class="btn-content"></div>
            </div>
        </div>
    </div>
</div>

<div class="info-detai-tour-head">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-12 col-lg-6">
                <div class="row no-gutters">
                    <div class="col-4 col-lg-3">
                        <div class="item-content-tour-head-mqn">
                            <div class="content-tour-head-mqn">
                                <div class="title">Price</div>
                                <div class="text">From <span>$114</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="item-content-tour-head-mqn">
                            <div class="img-content-tour d-none d-lg-block">
                                <img src="/img/clock-dura.svg" alt="icon">
                            </div>
                            <div class="content-tour-head-mqn">
                                <div class="title">Duration</div>
                                <div class="text">7 days</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="item-content-tour-head-mqn">
                            <div class="img-content-tour d-none d-lg-block" style="margin-top: -8px;">
                                <img src="/img/max-people.svg" alt="icon">
                            </div>
                            <div class="content-tour-head-mqn">
                                <div class="title">Max People</div>
                                <div class="text">30</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row no-gutters">
                    <div class="col-4 col-lg-4">
                        <div class="item-content-tour-head-mqn">
                            <div class="img-content-tour d-none d-lg-block" style="margin-top: -8px;">
                                <img src="/img/min-age.svg" alt="icon">
                            </div>
                            <div class="content-tour-head-mqn">
                                <div class="title">Min Age</div>
                                <div class="text">From</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-lg-5">
                        <div class="item-content-tour-head-mqn">
                            <div class="img-content-tour d-none d-lg-block">
                                <img src="/img/tour-type.svg" alt="icon">
                            </div>
                            <div class="content-tour-head-mqn">
                                <div class="title">Tour Type</div>
                                <div class="text">City Tours, Cruises</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-lg-3">
                        <div class="item-content-tour-head-mqn">
                            <div class="content-tour-head-mqn text-review">
                                <div class="title">Tour Type</div>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                <div class="text">8 Review</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="item-content-detail-tour">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="item-overview-tour">
                    <div class="title">Overview</div>
                    <div class="text-bold-italic">Set off on this Epic 7 day trip through Costa Rica's must see locations. From jungle to beach you will get to explore the amazing wildlife rich park of Manuel Antonio National Park, before heading off to the cloud forest of Monteverde, ending in the adventure packed town of La Fortuna at the base of Arenal National Park.</div>
                    <div class="text-italic">*This package trip requires a minimum of 2 people.</div>
                    <div class="text-italic">*Can be customized upon request, to meet travelers needs</div>
                    <div class="text-italic">*6 Nights Hotel/Lodge 3 Star accommodation (upgrades on request)</div>
                    <div class="text-italic">*Meals = (B) Breakfast</div>
                    <div class="text">Trip Extensions available on request.</div>
                </div>

                <div class="line-height"></div>

                <div class="included">
                    <div class="title">Included/Excluded</div>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="item-included">
                                <img src="/img/check.svg" alt="icon">
                                <div class="text">Specialized bilingual guide</div>
                            </div>
                            <div class="item-included">
                                <img src="/img/check.svg" alt="icon">
                                <div class="text">Private Transport</div>
                            </div>
                            <div class="item-included">
                                <img src="/img/check.svg" alt="icon">
                                <div class="text">Entrance fees (Cable and car and Moon Valley)</div>
                            </div>
                            <div class="item-included">
                                <img src="/img/check.svg" alt="icon">
                                <div class="text">Box lunch water, banana apple and chocolate</div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="item-included">
                                <img src="/img/close.svg" alt="icon">
                                <div class="text">Departure Taxes</div>
                            </div>
                            <div class="item-included">
                                <img src="/img/close.svg" alt="icon">
                                <div class="text">Entry Fees</div>
                            </div>
                            <div class="item-included">
                                <img src="/img/close.svg" alt="icon">
                                <div class="text">5 Star Accommodation</div>
                            </div>
                            <div class="item-included">
                                <img src="/img/close.svg" alt="icon">
                                <div class="text">Airport Transfers</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line-height"></div>

                <div class="tour-plan">
                    <div class="title">Tour Plan</div>

                    <div class="koh-tab-content active">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                                <p class="koh-faq-question-span">
                                    <span>Day 1 </span>
                                    ARRIVAL SAN JOSE - Drive to  MANUEL ANTONIO
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>

                    <div class="koh-tab-content">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                            <p class="koh-faq-question-span">
                                    <span>Day 2 </span>
                                    MANUEL ANTONIO 
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>

                    <div class="koh-tab-content">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                            <p class="koh-faq-question-span">
                                    <span>Day 3 </span>
                                    MONTEVERDE
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>

                    <div class="koh-tab-content">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                            <p class="koh-faq-question-span">
                                    <span>Day 4 </span>
                                    MONTEVERDE – ARENAL VOLCANO NATIONAL  
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>

                    <div class="koh-tab-content">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                            <p class="koh-faq-question-span">
                                    <span>Day 5 </span>
                                    ARENAL VOLCANO NATIONAL PARK 
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>

                    <div class="koh-tab-content">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                            <p class="koh-faq-question-span">
                                    <span>Day 6 </span>
                                    ARRIVAL SAN JOSE - Drive to  MANUEL ANTONIO
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>

                    <div class="koh-tab-content">
                         <div class="koh-faq-question">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            <img src="/img/map-toggle.svg" alt="icon">    
                            <p class="koh-faq-question-span">
                                    <span>Day 7 </span>
                                    ARENAL – DEPART SANJOSE (160km – 3 hrs) 
                                </p>
                        </div>
                        <div class="koh-faq-answer">
                            <div class="text">Upon arrival connect your rental car at SJO airport, head towards a stunning beachside destination, descending to Manuel Antonio National Park. On arrival, take the rest of the day to enjoy the surrounding areas, hotel facilities or the spectacular beach.</div>
                            <div class="text-bold">Overnight in Manuel Antonio</div>
                            <div class="text-bold">Drive: 3 hours</div>
                        </div>
                    </div>
                </div>

                <div class="line-height"></div>

                <div class="tour-map">
                    <div class="title">Tour Map</div>
                    @include('frontend.contact.includes.map')
                </div>

                <div class="line-height"></div>

                <div class="tour-relate-to">
                    <div class="title">You may like</div>
                    <div class="baner-slide-tour-relate">
                        <!-- item 1 -->
                        <div class="item">
                            <div class="item-tour">
                                <div class="content-header-tour">
                                    <img src="/img/tour1.png" alt="tour1" class="avata-tour">
                                    <div class="item-tag-time">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="row no-gutters">
                                                    <div class="col-7 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/calendar-solid.png" alt="calendar">
                                                            <div class="text">4 days</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/user-solid.png" alt="user">
                                                            <div class="text">10</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-info-tour">
                                    <div class="title">Caño Cristales River Trip</div>
                                    <div class="item-map">
                                        <img src="/img/map-icon.png" alt="">
                                        <div class="location">Bryce Canyon National Park, USA</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="price-tour">
                                                <div class="from">From</div>
                                                <div class="price">$80</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <a href="">
                                                <span>Explore Now</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-arrow-right color-orange"
                                                    viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd"
                                                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end item 1 -->

                        <!-- item 2 -->
                        <div class="item">
                            <div class="item-tour">
                                <div class="content-header-tour">
                                    <img src="/img/tour2.png" alt="tour1" class="avata-tour">
                                    <div class="item-tag-time">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="row no-gutters">
                                                    <div class="col-7 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/calendar-solid.png" alt="calendar">
                                                            <div class="text">4 days</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/user-solid.png" alt="user">
                                                            <div class="text">10</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-info-tour">
                                    <div class="title">Caño Cristales River Trip</div>
                                    <div class="item-map">
                                        <img src="/img/map-icon.png" alt="">
                                        <div class="location">Bryce Canyon National Park, USA</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="price-tour">
                                                <div class="from">From</div>
                                                <div class="price">$80</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <a href="">
                                                <span>Explore Now</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-arrow-right color-orange"
                                                    viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd"
                                                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end item 2 -->

                        <!-- item 3 -->
                        <div class="item">
                            <div class="item-tour">
                                <div class="content-header-tour">
                                    <img src="/img/tour3.png" alt="tour1" class="avata-tour">
                                    <div class="item-tag-time">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="row no-gutters">
                                                    <div class="col-7 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/calendar-solid.png" alt="calendar">
                                                            <div class="text">4 days</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/user-solid.png" alt="user">
                                                            <div class="text">10</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-info-tour">
                                    <div class="title">Caño Cristales River Trip</div>
                                    <div class="item-map">
                                        <img src="/img/map-icon.png" alt="">
                                        <div class="location">Bryce Canyon National Park, USA</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="price-tour">
                                                <div class="from">From</div>
                                                <div class="price">$80</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <a href="">
                                                <span>Explore Now</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-arrow-right color-orange"
                                                    viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd"
                                                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end item 3 -->

                        <!-- item 4 -->
                        <div class="item">
                        <div class="item-tour">
                                <div class="content-header-tour">
                                    <img src="/img/tour4.png" alt="tour1" class="avata-tour">
                                    <div class="item-tag-time">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="row no-gutters">
                                                    <div class="col-7 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/calendar-solid.png" alt="calendar">
                                                            <div class="text">4 days</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="/img/user-solid.png" alt="user">
                                                            <div class="text">10</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-info-tour">
                                    <div class="title">Caño Cristales River Trip</div>
                                    <div class="item-map">
                                        <img src="/img/map-icon.png" alt="">
                                        <div class="location">Bryce Canyon National Park, USA</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="price-tour">
                                                <div class="from">From</div>
                                                <div class="price">$80</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <a href="">
                                                <span>Explore Now</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-arrow-right color-orange"
                                                    viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd"
                                                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end item 4 -->
                    </div>
                </div>

                <div class="line-height"></div>

                <div class="review">
                    <div class="title">Reviews</div>
                    <div class="row">
                        <div class="col-12 col-lg-2">
                            <div class="rate-number">
                                <div class="number-rate"><span>4</span>/5</div>
                                <div class="status-rate">Very Good</div>
                                <div class="total-rate">8 verified reviews</div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5">
                            <div class="item-bar-mqn">
                                <div class="content-text">
                                    <div class="text">Location</div>
                                    <div class="number-text">4.13/5</div>
                                </div>
                                <div class="bar">
                                    <div class="bar-inner" data-percent="80%"></div>
                                </div>
                            </div>

                            <div class="item-bar-mqn">
                                <div class="content-text">
                                    <div class="text">Services</div>
                                    <div class="number-text">4.25/5</div>
                                </div>
                                <div class="bar">
                                    <div class="bar-inner" data-percent="85%"></div>
                                </div>
                            </div>

                            <div class="item-bar-mqn">
                                <div class="content-text">
                                    <div class="text">Rooms</div>
                                    <div class="number-text">4/5</div>
                                </div>
                                <div class="bar">
                                    <div class="bar-inner" data-percent="70%"></div>
                                </div>
                            </div>

                        
                        </div>
                        <div class="col-12 col-lg-5">
                            <div class="item-bar-mqn">
                                <div class="content-text">
                                    <div class="text">Amenities</div>
                                    <div class="number-text">3.88/5</div>
                                </div>
                                <div class="bar">
                                    <div class="bar-inner" data-percent="40%"></div>
                                </div>
                            </div>

                            <div class="item-bar-mqn">
                                <div class="content-text">
                                    <div class="text">Price</div>
                                    <div class="number-text">3.75/5</div>
                                </div>
                                <div class="bar">
                                    <div class="bar-inner" data-percent="35%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="item-comment-customer">
                        <div class="total-comment-customer">Showing 6–8 of 8 comments</div>
                    </div>

                    <!-- item comment 1 -->
                    <div class="item-list-comment">
                        <div class="row">
                            <div class="col-12 col-lg-1">
                                <img src="/img/avata-comment.png" alt="icon-cmt" class="avata-comment">
                            </div>
                            <div class="col-12 col-lg-11">
                                <div class="content-comment">
                                    <div class="name">Jonh Tery</div>
                                    <div class="datetime-comment">11/27/2020</div>
                                </div>
                                <div class="item-service-tour-detail">
                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Location</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-right">
                                        <div class="name-service">Location</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Services</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-right">
                                        <div class="name-service">Price</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Rooms</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                                <div class="form-comment-customer">
                                    <img src="/img/chart-cmt.svg" alt="icon-cmt">
                                    <div class="text">Reply</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End item comment 1 -->

                    <!-- item comment 2 -->
                    <div class="item-list-comment">
                        <div class="row">
                            <div class="col-12 col-lg-1">
                                <img src="/img/avata-comment.png" alt="icon-cmt" class="avata-comment">
                            </div>
                            <div class="col-12 col-lg-11">
                                <div class="content-comment">
                                    <div class="name">Jonh Tery</div>
                                    <div class="datetime-comment">11/27/2020</div>
                                </div>
                                <div class="item-service-tour-detail">
                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Location</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-right">
                                        <div class="name-service">Location</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Services</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-right">
                                        <div class="name-service">Price</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Rooms</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                                <div class="form-comment-customer">
                                    <img src="/img/chart-cmt.svg" alt="icon-cmt">
                                    <div class="text">Reply</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End item comment 2 -->

                    <!-- item comment 3 -->
                    <div class="item-list-comment">
                        <div class="row">
                            <div class="col-12 col-lg-1">
                                <img src="/img/avata-comment.png" alt="icon-cmt" class="avata-comment">
                            </div>
                            <div class="col-12 col-lg-11">
                                <div class="content-comment">
                                    <div class="name">Jonh Tery</div>
                                    <div class="datetime-comment">11/27/2020</div>
                                </div>
                                <div class="item-service-tour-detail">
                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Location</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-right">
                                        <div class="name-service">Location</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Services</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-right">
                                        <div class="name-service">Price</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>

                                    <div class="item-service-tour-detail-left">
                                        <div class="name-service">Rooms</div>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                            <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                                <div class="form-comment-customer">
                                    <img src="/img/chart-cmt.svg" alt="icon-cmt">
                                    <div class="text">Reply</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End item comment 3 -->
                    <div class="item-button-old-comment">
                        <button>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left color-orange" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                            </svg>
                            <span>Explore Now</span>
                            
                        </button>
                    </div>
                </div>

                <div class="line-height"></div>

                <div class="form-contact-main">
                    <div class="title">Leave a Reply</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <input type="text" class="form-control"  placeholder="Your name *" required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <input type="email" class="form-control"  placeholder="Your mail *" required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <input type="url" class="form-control"  placeholder="Your Website *" required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-8">
                                <div class="form-group">
                                    <textarea name=""  class="form-control" placeholder="Messager *" cols="30" rows="10" required></textarea>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="item-rateting-form">
                                        <div class="item-text-rateting">Location</div>
                                        <div class="half-stars-example">
                                        <div class="rating-group">
                                            <input class="rating__input rating__input--none" checked name="rating2" id="rating2-0" value="0" type="radio">
                                            <label aria-label="0 stars" class="rating__label" for="rating2-0">&nbsp;</label>
                                            <label aria-label="0.5 stars" class="rating__label rating__label--half" for="rating2-05"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-05" value="0.5" type="radio">
                                            <label aria-label="1 star" class="rating__label" for="rating2-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-10" value="1" type="radio">
                                            <label aria-label="1.5 stars" class="rating__label rating__label--half" for="rating2-15"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-15" value="1.5" type="radio">
                                            <label aria-label="2 stars" class="rating__label" for="rating2-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-20" value="2" type="radio">
                                            <label aria-label="2.5 stars" class="rating__label rating__label--half" for="rating2-25"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-25" value="2.5" type="radio" >
                                            <label aria-label="3 stars" class="rating__label" for="rating2-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-30" value="3" type="radio">
                                            <label aria-label="3.5 stars" class="rating__label rating__label--half" for="rating2-35"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-35" value="3.5" type="radio">
                                            <label aria-label="4 stars" class="rating__label" for="rating2-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-40" value="4" type="radio">
                                            <label aria-label="4.5 stars" class="rating__label rating__label--half" for="rating2-45"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-45" value="4.5" type="radio">
                                            <label aria-label="5 stars" class="rating__label" for="rating2-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating2" id="rating2-50" value="5" type="radio" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-rateting-form">
                                        <div class="item-text-rateting">Amenities</div>
                                        <div class="half-stars-example">
                                        <div class="rating-group">
                                            <input class="rating__input rating__input--none" checked name="rating3" id="rating3-0" value="0" type="radio">
                                            <label aria-label="0 stars" class="rating__label" for="rating3-0">&nbsp;</label>
                                            <label aria-label="0.5 stars" class="rating__label rating__label--half" for="rating3-05"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-05" value="0.5" type="radio">
                                            <label aria-label="1 star" class="rating__label" for="rating3-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-10" value="1" type="radio">
                                            <label aria-label="1.5 stars" class="rating__label rating__label--half" for="rating3-15"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-15" value="1.5" type="radio">
                                            <label aria-label="2 stars" class="rating__label" for="rating3-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-20" value="2" type="radio">
                                            <label aria-label="2.5 stars" class="rating__label rating__label--half" for="rating3-25"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-25" value="2.5" type="radio" >
                                            <label aria-label="3 stars" class="rating__label" for="rating3-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-30" value="3" type="radio">
                                            <label aria-label="3.5 stars" class="rating__label rating__label--half" for="rating3-35"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-35" value="3.5" type="radio">
                                            <label aria-label="4 stars" class="rating__label" for="rating3-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-40" value="4" type="radio">
                                            <label aria-label="4.5 stars" class="rating__label rating__label--half" for="rating3-45"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-45" value="4.5" type="radio">
                                            <label aria-label="5 stars" class="rating__label" for="rating3-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating3" id="rating3-50" value="5" type="radio" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-rateting-form">
                                        <div class="item-text-rateting">Services</div>
                                        <div class="half-stars-example">
                                        <div class="rating-group">
                                            <input class="rating__input rating__input--none" checked name="rating4" id="rating4-0" value="0" type="radio">
                                            <label aria-label="0 stars" class="rating__label" for="rating4-0">&nbsp;</label>
                                            <label aria-label="0.5 stars" class="rating__label rating__label--half" for="rating4-05"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-05" value="0.5" type="radio">
                                            <label aria-label="1 star" class="rating__label" for="rating4-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-10" value="1" type="radio">
                                            <label aria-label="1.5 stars" class="rating__label rating__label--half" for="rating4-15"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-15" value="1.5" type="radio">
                                            <label aria-label="2 stars" class="rating__label" for="rating4-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-20" value="2" type="radio">
                                            <label aria-label="2.5 stars" class="rating__label rating__label--half" for="rating4-25"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-25" value="2.5" type="radio" >
                                            <label aria-label="3 stars" class="rating__label" for="rating4-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-30" value="3" type="radio">
                                            <label aria-label="3.5 stars" class="rating__label rating__label--half" for="rating4-35"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-35" value="3.5" type="radio">
                                            <label aria-label="4 stars" class="rating__label" for="rating4-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-40" value="4" type="radio">
                                            <label aria-label="4.5 stars" class="rating__label rating__label--half" for="rating4-45"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-45" value="4.5" type="radio">
                                            <label aria-label="5 stars" class="rating__label" for="rating4-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating4" id="rating4-50" value="5" type="radio" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-rateting-form">
                                        <div class="item-text-rateting">Price</div>
                                        <div class="half-stars-example">
                                        <div class="rating-group">
                                            <input class="rating__input rating__input--none" checked name="rating5" id="rating5-0" value="0" type="radio">
                                            <label aria-label="0 stars" class="rating__label" for="rating5-0">&nbsp;</label>
                                            <label aria-label="0.5 stars" class="rating__label rating__label--half" for="rating5-05"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-05" value="0.5" type="radio">
                                            <label aria-label="1 star" class="rating__label" for="rating5-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-10" value="1" type="radio">
                                            <label aria-label="1.5 stars" class="rating__label rating__label--half" for="rating5-15"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-15" value="1.5" type="radio">
                                            <label aria-label="2 stars" class="rating__label" for="rating5-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-20" value="2" type="radio">
                                            <label aria-label="2.5 stars" class="rating__label rating__label--half" for="rating5-25"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-25" value="2.5" type="radio" >
                                            <label aria-label="3 stars" class="rating__label" for="rating5-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-30" value="3" type="radio">
                                            <label aria-label="3.5 stars" class="rating__label rating__label--half" for="rating5-35"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-35" value="3.5" type="radio">
                                            <label aria-label="4 stars" class="rating__label" for="rating5-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-40" value="4" type="radio">
                                            <label aria-label="4.5 stars" class="rating__label rating__label--half" for="rating5-45"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-45" value="4.5" type="radio">
                                            <label aria-label="5 stars" class="rating__label" for="rating5-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating5" id="rating5-50" value="5" type="radio" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-rateting-form">
                                        <div class="item-text-rateting">Rooms</div>
                                        <div class="half-stars-example">
                                        <div class="rating-group">
                                            <input class="rating__input rating__input--none" checked name="rating6" id="rating6-0" value="0" type="radio">
                                            <label aria-label="0 stars" class="rating__label" for="rating6-0">&nbsp;</label>
                                            <label aria-label="0.5 stars" class="rating__label rating__label--half" for="rating6-05"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-05" value="0.5" type="radio">
                                            <label aria-label="1 star" class="rating__label" for="rating6-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-10" value="1" type="radio">
                                            <label aria-label="1.5 stars" class="rating__label rating__label--half" for="rating6-15"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-15" value="1.5" type="radio">
                                            <label aria-label="2 stars" class="rating__label" for="rating6-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-20" value="2" type="radio">
                                            <label aria-label="2.5 stars" class="rating__label rating__label--half" for="rating6-25"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-25" value="2.5" type="radio" >
                                            <label aria-label="3 stars" class="rating__label" for="rating6-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-30" value="3" type="radio">
                                            <label aria-label="3.5 stars" class="rating__label rating__label--half" for="rating6-35"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-35" value="3.5" type="radio">
                                            <label aria-label="4 stars" class="rating__label" for="rating6-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-40" value="4" type="radio">
                                            <label aria-label="4.5 stars" class="rating__label rating__label--half" for="rating6-45"><i class="rating__icon rating__icon--star fa fa-star-half"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-45" value="4.5" type="radio">
                                            <label aria-label="5 stars" class="rating__label" for="rating6-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <input class="rating__input" name="rating6" id="rating6-50" value="5" type="radio" checked>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="comment">
                                    <label class="form-check-label" for="comment">Your email address will not be published. Required fields are marked *</label>
                                </div>
                            </div>
                           <div class="col-12">
                            <button type="submit" class="btn">Post Comment</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="col-12 col-lg-4">
                <div class="slide-right-detail-tour-mqn">
                    <div class="banner-book-tour">
                        <div class="item-taitho-phone"></div>
                        <div class="title">Book This Tour</div>

                        <form action="" method="post">
                            <div class="form-option-time">
                                <div class="text">Form:</div>
                                <div class="form-group">
                                        <input type="date" class="form-control" name="" value="" required>
                                </div>
                            </div>
                            <div class="form-option-time">
                                <div class="text-time">Time:</div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="" id="inlineRadio1" value="10:00">
                                    <label class="form-check-label" for="inlineRadio1">10:00</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="" id="inlineRadio2" value="12:00">
                                    <label class="form-check-label" for="inlineRadio2">12:00</label>
                                </div>
                            </div>

                            <div class="form-option-tickets">
                                <div class="title-tickets">Tickets:</div>
                                <div class="chose-number-user">
                                    <div class="item-chose-user">
                                        <div class="text">Adult (18+ years</div>
                                        <div class="price">$114</div>
                                    </div>
                                    <div class="item-sort-tour-mqn">
                                        <div class="form-group">
                                            <select class="form-control" id="exampleFormControlSelect1">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="chose-number-user">
                                    <div class="item-chose-user">
                                        <div class="text">Youth (13-17 years) </div>
                                        <div class="price">$114</div>
                                    </div>
                                    <div class="item-sort-tour-mqn">
                                        <div class="form-group">
                                            <select class="form-control" id="exampleFormControlSelect1">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="chose-number-user">
                                    <div class="item-chose-user">
                                        <div class="text">Children (0-12 years)</div>
                                        <div class="price">$114</div>
                                    </div>
                                    <div class="item-sort-tour-mqn">
                                        <div class="form-group">
                                            <select class="form-control" id="exampleFormControlSelect1">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-option-tickets">
                                <div class="title-tickets">Add Extra:</div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                            Service per booking
                                    </label>
                                </div>

                                <div class="form-check bd-none">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                                    <label class="form-check-label" for="defaultCheck2">
                                            Service per person
                                    </label>
                                </div>

                                <div class="text-mqn">Adult: <span>$17</span></div>
                                <div class="text-mqn">Adult: <span>$17</span></div>
                            </div>

                            <div class="form-option-time bd-none">
                                <div class="text-time">Total:</div>
                                <div class="total-price">$228</div>
                            </div>

                            <button type="submit" name="" class="submit">Book now</button>
                        </form>
                    </div>

                    <div class="banner-recent-post">
                        <div class="title">Recent Posts</div>
                        <div class="list-tour-recent-post">
                           <div class="item-tour-recent-post">
                            <div class="avata-recent-post">
                                    <img src="/img/tour8.png" alt="">
                                </div>
                                <div class="content-recent-post">
                                    <div class="title-recent-post">Pack wisely before ...</div>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <div class="price-recent-post">$117</div>
                                </div>
                           </div>
                           <div class="item-tour-recent-post">
                                <div class="avata-recent-post">
                                    <img src="/img/tour9.png" alt="">
                                </div>
                                <div class="content-recent-post">
                                    <div class="title-recent-post">Adventure Colombia...</div>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <div class="price-recent-post">$93</div>
                                </div>
                           </div>
                           <div class="item-tour-recent-post">
                                <div class="avata-recent-post">
                                    <img src="/img/tour10.png" alt="">
                                </div>
                                <div class="content-recent-post">
                                    <div class="title-recent-post">Discovery Islands...</div>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <i class="fa fa-star green"></i>
                                    <div class="price-recent-post">$205</div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>