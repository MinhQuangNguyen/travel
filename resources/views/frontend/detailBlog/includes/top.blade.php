<div class="header-sub-title-page-mqn">
    <div class="container-fluid">
        <div class="content-title-sub-page">
            <div class="title">Blog</div>
            <div class="sub-title">
                <a href="">Pack wisely before traveling</a>
            </div>
        </div>
    </div>
</div>


<div class="blog-main-mqn">
    <div class="container-fluid">
        <div class="row">
            @include('frontend.detailBlog.includes.itemBlogDetail')
            @include('frontend.detailBlog.includes.slideBar')
        </div>
    </div>
</div>
