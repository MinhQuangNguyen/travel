<div class="col-12 col-lg-4 d-none d-lg-block">
    <div class="item-list-blog-search">
        <form action="" method="post">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon2" required>
                <div class="input-group-append">
                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </form>
    </div>

    <div class="item-list-blog-categories">
        <div class="title-item">Blog Categories</div>
        <div class="list-item-popular-tour">
            <div class="row">
                <div class="col-12">
                    <a href="">
                        <div class="item-popular-mqn-lala">
                            <div class="row">
                                <div class="col-9">
                                    <div class="item-title-popular">
                                        <img src="/img/right-arrow-Popular.svg" alt="icon">
                                        <div class="title-poplar-blog">Blog Categories</div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="number-list">
                                        <span>3</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12">
                    <a href="">
                        <div class="item-popular-mqn-lala">
                            <div class="row">
                                <div class="col-9">
                                    <div class="item-title-popular">
                                        <img src="/img/right-arrow-Popular.svg" alt="icon">
                                        <div class="title-poplar-blog">Company Insight</div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="number-list">
                                        <span>2</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12">
                    <a href="">
                        <div class="item-popular-mqn-lala">
                            <div class="row">
                                <div class="col-9">
                                    <div class="item-title-popular">
                                        <img src="/img/right-arrow-Popular.svg" alt="icon">
                                        <div class="title-poplar-blog">Creative</div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="number-list">
                                        <span>5</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12">
                    <a href="">
                        <div class="item-popular-mqn-lala">
                            <div class="row">
                                <div class="col-9">
                                    <div class="item-title-popular">
                                        <img src="/img/right-arrow-Popular.svg" alt="icon">
                                        <div class="title-poplar-blog">Lifestyle</div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="number-list">
                                        <span>7</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12">
                    <div class="item-popular-mqn-lala">
                        <div class="row">
                            <div class="col-9">
                                <div class="item-title-popular">
                                    <img src="/img/right-arrow-Popular.svg" alt="icon">
                                    <div class="title-poplar-blog">Tips & Tricks</div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="number-list">
                                    <span>2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="item-popular-mqn-lala">
                        <div class="row">
                            <div class="col-9">
                                <div class="item-title-popular">
                                    <img src="/img/right-arrow-Popular.svg" alt="icon">
                                    <div class="title-poplar-blog">Uncategorized</div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="number-list">
                                    <span>3</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="item-list-blog-recent-post">
        <div class="title-item">Recent Posts</div>
        <div class="item-blog-popular-mqn-slidebar">
            <div class="avata-poppular-blog-slidebar">
                <img src="/img/blog2.png" alt="hinhanh">
            </div>
            <div class="content-blog-poppular-blog-slidebar">
                <a href="">
                    <div class="title">Pack wisely before traveling</div>
                    <div class="time">09/06/2021</div>
                </a>
            </div>
        </div>

        <div class="item-blog-popular-mqn-slidebar">
            <div class="avata-poppular-blog-slidebar">
                <img src="/img/blog1.png" alt="hinhanh">
            </div>
            <div class="content-blog-poppular-blog-slidebar">
                <a href="">
                    <div class="title">The Surfing Man Will Your...</div>
                    <div class="time">09/06/2021</div>
                </a>
            </div>
        </div>

        <div class="item-blog-popular-mqn-slidebar">
            <div class="avata-poppular-blog-slidebar">
                <img src="/img/blog3.png" alt="hinhanh">
            </div>
            <div class="content-blog-poppular-blog-slidebar">
                <a href="">
                    <div class="title">Separated they live </div>
                    <div class="time">09/06/2021</div>
                </a>
            </div>
        </div>
    </div>
    
    <div class="item-list-blog-popular-tags">
        <div class="title-item">Popular Tag</div>
        <div class="tags-popular-mqn-blog-pro">
            <ul>
                <li>
                    <a href="">
                        <span>Bread</span>
                   </a>
                </li>
                <li>
                    <a href="">
                        <span>Fruits</span>
                   </a>
                </li>
                <li>
                    <a href="">
                        <span>Meat</span>
                   </a>
                </li>
                <li>
                    <a href="">
                        <span>natural</span>
                   </a>
                </li>
                <li>
                    <a href="">
                        <span>vegetables</span>
                       </a>
                </li>
                <li>
                    <a href="">
                        <span>Bread</span>
                   </a>
                </li>
                <li>
                    <a href="">
                        <span>Fruits</span>
                   </a>
                </li>
                <li>
                    <a href="">
                        <span>Meat</span>
                   </a>
                </li>
            </ul>
        </div>
    </div>
</div>