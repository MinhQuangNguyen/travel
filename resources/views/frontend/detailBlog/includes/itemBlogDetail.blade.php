<div class="col-12 col-lg-8">
    <div class="item-blog-detail-mqn">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="tags">Company Insight</div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="title-main-blog">The Surfing Man Will Blow Your Mind</div>
        <img src="/img/blog2.png" alt="hinhanh" class="avata-blog-main">
        <div class="text">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of
            spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which
            was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
            sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single
            stroke at the present moment; and yet I feel that I never was a greater artist than now.</div>
        <div class="text-b">When, while the lovely valley teems with</div>
        <div class="text">vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage
            of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall
            grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by
            me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless
            indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his
            own image, and the breath of that universal love which bears and sustains us, as it floats around us in an
            eternity of blist.</div>
        <div class="quote">
            <img src="/img/icon-quote-mini.png" alt="icon-quote">
            <div class="text-quote">I sink under the weight of the splendour of these visions!A wonderful serenity has
                taken possession of my entire soul, like these sweet mornings of spring which</div>
        </div>
        <div class="text">I sink under the weight of the splendour of these visions!A wonderful serenity has taken
            possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am
            alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I
            am so happy, my dear friend, so absorbed in the exquis</div>
        <div class="text-b">I throw myself down among the tall grass</div>
        <div class="text">I should be incapable of drawing a single stroke at the present moment; and yet I feel that I
            never was a greater artist than now. When, while the lovely valley teems with vapour around me, and the
            meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams
            steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I
            lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little
            world among the stalks, and grow familiar with the countless indescribable forms of the insects and</div>

        <img src="/img/blog5.png" alt="hinhanh" class="img-detail-blog">
        <div class="text-b">Text, that where it came from it</div>
        <div class="text">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there
            live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large
            language ocean. A small river named Duden flows by their place and supplies it with the necessary
            regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the
            all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day
            however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.
            The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and
            devious Semikoli, but the Littl</div>
        <ul>
            <li>Far far away, behind the word mountain</li>
            <li>When she reached the first hills</li>
            <li>A small river named Duden flows</li>
            <li>A small river named Duden flows by their plat.</li>
            <li>Far far away, behind the word mountain</li>
        </ul>
        <div class="text">Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their
            agency, where they abused her for their projects again and again. And if she hasn’t been rewritten, then
            they are still using her.Far far away, behind the word mountains, far from the countries Vokalia and
            Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the
            Semantics, a large language ocean. A small river named Duden flows by their plate.</div>
    </div>
    <div class="prev-post-detail-blog">
        <div class="natural">natural</div>
        <div class="prev-post-blog-item">
            <img src="/img/arrow-prev-blog.svg" alt="icon-arrow">
            <a href="">
                <div class="content-blog-prev">
                    <div class="sub-prev">prve post</div>
                    <div class="title-blog">Pack wisely before...</div>
                </div>
            </a>
        </div>
    </div>
    <div class="comment-blog-mqn">
        <div class="title">Leave a Reply</div>
        <div class="text-decs">Your email address will not be published. Required fields are marked *</div>
        <form action="" method="POST">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your name *" required>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Address *" required>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Website *" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <textarea name="" class="form-control" placeholder="Messager *" cols="30" rows="10"
                            required></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="comment">
                        <label class="form-check-label" for="comment">Your email address will not be published. Required
                            fields are marked *</label>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn">Post Comment</button>
                </div>
            </div>
        </form>
    </div>
</div>
