@extends('frontend.layouts.alien')

@section('title', __('News'))

@section('content')
@include('frontend.news.includes.headerSubPage')
@include('frontend.news.includes.listBlog')

@endsection
