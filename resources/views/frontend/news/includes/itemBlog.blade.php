<div class="col-12 col-lg-8">
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog1.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog2.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog3.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog4.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog5.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog6.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog7.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="item-blog-main-mqn">
                <div class="img-tags-blog">
                    <img src="/img/blog8.png" alt="hinhanh">
                    <div class="tags">Company Insight</div>
                </div>
                <div class="content-list-blog-mqn">
                    <div class="info-item-blog-mqn">
                        <div class="item-info-blog">
                            <img src="/img/calendar-solid.png" alt="">
                            <div class="text-info-blog">08/06/2021</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/user-solid.png" alt="">
                            <div class="text-info-blog"><span>By</span> Admin</div>
                        </div>
                        <div class="item-info-blog">
                            <img src="/img/comment-solid.png" alt="">
                            <div class="text-info-blog">20</div>
                        </div>
                    </div>
                    <div class="title-blog">The Surfing Man Will Blow Your Mind</div>
                    <div class="text-decs">A wonderful serenity has taken possession of my entire soul, like
                        these sweet mornings of spring…</div>
                    <a href="">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>