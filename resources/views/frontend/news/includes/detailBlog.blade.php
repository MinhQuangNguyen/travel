<div class="detail-blog-main-alien-mqn">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-12 col-lg-8">
                <div class="item-content-info-detail-blog">
                    {!! $post->content !!}
                </div>
                <div class="item-new-related">
                    <div class="title">bài viết liên quan</div>
                </div>

                <div class="partner-home-slide-group">
                    <div class="partner-home-slide">
                        @if(isset($news_relates))
                        @foreach($news_relates as $key => $item)
                        <div class="item">
                            <div class="item-list-blog-mqn-main">
                                <a
                                    href="{{ $item->category->getLink().'/'.$item->category->slug.'/'.$item->slug.'.html' }}">
                                    <div class="item-blog-alien-mqn"
                                        style="background-image: url('{{ asset( $item->image) }}')">
                                    </div>
                                    <div class="content-info-title-blog-mqn">
                                        <div class="tag-blog">{{ $item->tagser }}</div>
                                        <div class="title-item-blog">
                                            <h1>{{ $item->title }}</h1>
                                            <div class="row d-flex justify-content-between">
                                                <div class="col-12 col-lg-7 d-none d-lg-block">
                                                    <div class="item-icon-option">
                                                        <img src="/img/icon-alien.svg" alt="hinhanh">
                                                        <div class="text">{{ $item->author }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-5">
                                                    <div class="item-icon-option">
                                                        <span><img src="/img/icon-clock.svg"
                                                                alt="hinhanh">{{ date_format($item->created_at, 'd/m/Y') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>

                    <div class="btn-kc btn-back partner-prev">
                        <div class="btn-content"></div>
                    </div>
                    <div class="btn-kc btn-next partner-next">
                        <div class="btn-content"></div>
                    </div>
                </div>

            </div>

            <div class="col-12 col-lg-4">
                {!! $setting['newCourser'] !!}

                {!! $setting['behance'] !!}

                {{-- <div class="item-behance-mqn-main">
                    <div class="title">Dự án mới trên <i class="fab fa-behance"></i></i></div>

                    <div class="item-project-behance">
                        <div class="iframely-embed"><div class="iframely-responsive" style="padding-bottom: 56.2857%; padding-top: 120px;"><a href="https://www.behance.net/gallery/116008635/Website-Lamborghini-Aventador" data-iframely-url="//cdn.iframe.ly/9PbF4bO"></a></div></div>
                    </div>

                    <div class="item-project-behance">
                        <div class="iframely-embed"><div class="iframely-responsive" style="padding-bottom: 75%; padding-top: 120px;"><a href="https://www.behance.net/gallery/114078209/App-Fitness" data-iframely-url="//cdn.iframe.ly/vLkKwJj"></a></div></div>
                    </div>

                    <div class="item-project-behance">
                        <div class="iframely-embed"><div class="iframely-responsive" style="padding-bottom: 75%; padding-top: 120px;"><a href="https://www.behance.net/gallery/111829375/App-Booking-Travel" data-iframely-url="//cdn.iframe.ly/zWcCZ3P"></a></div></div>
                    </div>
                </div> --}}

                <div class="let-get-social-mqn-main">
                    <div class="title">Let's get social</div>
                    <div class="icon-social-form">
                        <ul>
                            <li>
                                <a href="{!! $setting['link_facebook'] !!}" target="_blank">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-facebook-f fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="{!! $setting['link_behance'] !!}" target="_blank">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-behance fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="{!! $setting['link_youtube'] !!}" target="_blank">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-youtube fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="{!! $setting['link_ins'] !!}" target="_blank">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-instagram fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>
