<div class="blog-main-mqn">
    <div class="container-fluid">
        <div class="row">
            @include('frontend.news.includes.itemBlog')
            @include('frontend.news.includes.slideBar')
        </div>
    </div>
</div>

<div class="pagination-mqn">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <ul class="pagination">
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">NEXT <img src="/img/icon-arrow-right.svg" alt="icon-right"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
