<div class="row">
    @foreach($posts as $post)
        <div class="col-12 col-lg-4">
            <div class="item-list-blog-mqn-main">
                <a href="/tin-tuc/{{ $post->category->slug.'/'.$post->slug.'.html' }}">
                    <div class="item-blog-alien-mqn" style="background-image: url('{{ asset($post->image) }}')"></div>
                    <div class="content-info-title-blog-mqn">
                        <div class="tag-blog">{{ $post->tagser }}</div>
                        <div class="title-item-blog">
                            <h1>{{ $post->title }}</h1>
                            <div class="row d-flex justify-content-between">
                                <div class="col-12 col-lg-7 d-none d-lg-block">
                                    <div class="item-icon-option">
                                        <img src="/img/icon-alien.svg" alt="hinhanh">
                                        <div class="text">{{ $post->author }}</div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <div class="item-icon-option">
                                        <span><img src="/img/icon-clock.svg" alt="hinhanh">{{ date_format($post->created_at, 'd/m/Y') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    @endforeach

            
    <div class="col-12 col-lg-12">
        {{ $posts->fragment('news-srcoll')->links() }}
    </div>
</div>


