<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="follow" />
    <meta name="description"
        content="DUI provides a simple, customizable, and accessible library of Laravel components. Follow your own design system, or start with Dreamy UI Design." />
    <meta name="keywords" content="System Design UI/UX, Library Laravel, Dreamy UI" />
    <meta name="author" content="@yield('meta_author', 'Dreamy UI')">
    <meta name="copyright" content="Media Iconic" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=3.0">
    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:description" name="description" content="System Design DM." />
    <meta property="og:site_name" content="{{ appURL() }}" />
    <meta property="og:image" content="/img/meta.png" />
    <link rel="icon" sizes="96x96" type="image/gif" href="/img/flaticon-96x96.png" />
    <!-- Chrome, Firefox OS and Opera -->     
    <meta name="theme-color" content="var(--lightBg)">     
    <!-- Windows Phone -->     
    <meta name="msapplication-navbutton-color" content="var(--lightBg)">     
    <!-- iOS Safari -->     
    <meta name="apple-mobile-web-app-status-bar-style" content="var(--lightBg)">
    <link href="https://fonts.googleapis.com/css2?family=Be+Vietnam+Pro:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    @yield('meta')

    @stack('before-styles')
    <link href="{{ mix('css/frontend.min.css') }}" rel="stylesheet" fetchpriority='low'>
    <link href="{{ mix('css/style.css') }}" rel="stylesheet" fetchpriority='high'>
    @stack('after-styles')

</head>

<body data-spy="scroll" data-target="#myScrollspy" data-offset="1">
    <div id="app">
        {{-- @include('includes.partials.messages') --}}

        <main>
            <div class="content-body-componentDM">
                @yield('content')
                @include('frontend.component.includes.ModeLang')
            </div>
        </main>
    </div>

    @stack('before-scripts')
    <script src="{{ mix('js/component/MainDui.js') }}" crossorigin="anonymous" fetchpriority='high'></script>
    <script src="{{ mix('js/component/supportDUi.js') }}" crossorigin="anonymous" fetchpriority='high'></script>
    @stack('after-scripts')


    
</body>
</html>
