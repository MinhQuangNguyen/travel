<div class="contact-hotline">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="info-contact-hotline">
                    <div class="text">Don’t wait any longer. Contact us!</div>
                    <div class="item-contact-mqn">
                        <img src="/img/support.png" alt="icon" class="icon-contact">
                        <div class="text-contact">(+84).345.736.211</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="info-contact-hotline">
                    <div class="item-contact-mqn">
                        <img src="/img/mail.png" alt="icon" class="icon-contact">
                        <div class="text-contact">support@gmail.com</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="info-contact-hotline">
                    <div class="text">Follow us</div>
                    <ul>
                        <li><a href=""><img src="/img/icon-facebook.png" alt="icon"></a></li>
                        <li><a href=""><img src="/img/icon-ig.png" alt="icon"></a></li>
                        <li><a href=""><img src="/img/icon-tw.png" alt="icon"></a></li>
                        <li><a href=""><img src="/img/icon-youtube.png" alt="icon"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="about-footer">
                    <img src="/img/logo-footer.png" alt="logo" class="logo-footer">
                    <div class="text">Nisi ut aliquip ex ea commodo consequatute irure dolor in reprehenderit in
                        voluptatevelit esse cillum dolore eu fugiat nulla excepteur pariatur.</div>
                    <img src="/img/Pin.svg" alt="map" class="icon-map">
                    <div class="address">Tòa Zentower, 12 Khuat Duy Tien, Thanh Xuan, Ha Noi</div>
                    <a href="">
                        <span>View Map</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-blue" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="row">
                    <div class="col-6">
                        <div class="menu-footer">
                            <div class="title-menu">Our Services</div>
                            <ul>
                                <li><a href="">Booking</a></li>
                                <li><a href="">RentalCar</a></li>
                                <li><a href="">HostelWorld</a></li>
                                <li><a href="">Trivago</a></li>
                                <li><a href="">TripAdvisor</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="menu-footer">
                            <div class="title-menu">Explore</div>
                            <ul>
                                <li><a href="">Madrid Tour</a></li>
                                <li><a href="">Stockholm City</a></li>
                                <li><a href="">Roma City</a></li>
                                <li><a href="">Shanghai City</a></li>
                                <li><a href="">Tokyo</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="form-contact-footer">
                    <div class="sub-title">Get Updates & More</div>
                    <div class="text">Subscribe to the free newsletter and stay up to date</div>
                    <form action="" method="POST">
                        <div class="row no-gutters">
                            <div class="col-7 col-lg-12">
                                <input type="text" name="" value="" placeholder="Your Email" required>
                            </div>
                            <div class="col-5 col-lg-12">
                                <button type="submit" name="">
                                    <span>Subscribe</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-arrow-right color-white" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="copyright">
    <div class="container-fluid">
        <div class="text">Copyright © 2021 <span>Triply</span>. All Rights Reserved.</div>
    </div>
</div>