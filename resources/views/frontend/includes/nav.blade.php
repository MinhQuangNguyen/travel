<div class="header-menu">
    <nav class="navbar navbar-expand-lg navbar-light justify-content-between customer-menu-pc">
        <div class="container-fluid">
            <img src="/img/menu1.svg" onclick="openNav()" class="top-menu menu1 d-block d-lg-none" alt="menu1">
            <a class="navbar-brand" href="/">
                <div class="logo">
                    <img src="{{ asset('img/logo-header.png') }}" alt="hinhanh" />
                </div>
            </a>
            <img src="/img/Person.svg" class="top-menu menu3 d-block d-lg-none" alt="menu3">
            <img src="/img/menu2.svg" onclick="openInfo()" class="top-menu menu2 d-block d-lg-none" alt="menu2">
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav">
                    @if(isset($menu))
                    @foreach ($menu as $menuItem)
                    @if ($menuItem->hasChild())
                    <li
                        class="nav-item {{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}">
                        <a class="nav-link" href="{{$menuItem->clickable ? $menuItem->link: '#'}}">
                            {{ $menuItem->name }} <span class="cham">
                        </a>
                    </li>
                    @else
                    <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
                    <li class="nav-item {{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
                        <a class="nav-link" href="{{ $menuItem->link }}">
                            {{ $menuItem->name }} <span class="cham">
                        </a>
                    </li>
                    @endif
                    @endforeach
                    @endif

                </ul>
                <div class="d-none d-lg-block">
                    <div class="login-contact-mqn">
                        <div class="telephone-header-mqn">
                            <img src="/img/tele-phone.svg" alt="tele-phone" class="tele-phone">
                            <div class="phone-number-mqn">(+84).345.768.211</div>
                        </div>
                        <div class="header-contact-login-mqn">
                            <img src="/img/Person.svg" alt="person" class="person-login">
                            <img src="/img/menu2.svg" onclick="openInfoPC()" class="top-menu menu2" alt="menu2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>


@include("frontend.includes.menu-mobile")


<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "80%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    function openInfo() {
        document.getElementById("myInfoHeader").style.width = "80%";
    }

    function closeInfo() {
        document.getElementById("myInfoHeader").style.width = "0";
    }

    function openInfoPC() {
        document.getElementById("myInfoHeaderPC").style.width = "30%";
    }

    function closeInfoPC() {
        document.getElementById("myInfoHeaderPC").style.width = "0";
    }


</script>
