<div id="mySidenav" class="menuMobile">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    @if(isset($menu))
    @foreach ($menu as $menuItem)
    @if ($menuItem->hasChild())
    <a href="{{$menuItem->clickable ? $menuItem->link: '#'}}" class="{{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}">
        {{ $menuItem->name }}
    </a>
    @else
    <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
    <a href="{{ $menuItem->link }}" class="{{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
        {{ $menuItem->name }}
    </a>
    @endif
    @endforeach
    @endif
  </div>


  <div id="myInfoHeader" class="infoHeaderMenu">
    <a href="javascript:void(0)" class="closebtn" onclick="closeInfo()">&times;</a>
    <div class="header-info-menu-top">
        <img src="/img/logo-header.png" alt="logo" class="logo-info-menu">
        <div class="text">Caño Cristales Tour, (aka the most beautiful river in the world) is an exceptional and surprising natural beauty. Its unique system is very fragile and now belongs to the Sierra...</div>
        <div class="item-contact-address">
            <div class="sub-title">Address</div>
            <div class="text-contact-info">Tòa Zentower, 12 Khuat Duy Tien, Thanh Xuan, Ha Noi</div>
        </div>
        <div class="item-contact-address">
            <div class="sub-title">Phone</div>
            <div class="text-contact-info">(+84).345.736.211</div>
        </div>
        <div class="item-contact-address">
            <div class="sub-title">Email</div>
            <div class="text-contact-info"><span>hotro@mediaiconic.com</span></div>
        </div>
        <ul>
            <li><a href=""><img src="/img/icon-facebook.png" alt="facebook"></a></li>
            <li><a href=""><img src="/img/icon-youtube.png" alt="youtube"></a></li>
            <li><a href=""><img src="/img/icon-tw.png" alt="tw"></a></li>
            <li><a href=""><img src="/img/icon-ig.png" alt="ig"></a></li>
        </ul>
    </div>
  </div>

  <div id="myInfoHeaderPC" class="infoHeaderMenu">
    <a href="javascript:void(0)" class="closebtn" onclick="closeInfoPC()">&times;</a>
    <div class="header-info-menu-top">
        <img src="/img/logo-header.png" alt="logo" class="logo-info-menu">
        <div class="text">Caño Cristales Tour, (aka the most beautiful river in the world) is an exceptional and surprising natural beauty. Its unique system is very fragile and now belongs to the Sierra...</div>
        <div class="item-contact-address">
            <div class="sub-title">Address</div>
            <div class="text-contact-info">Tòa Zentower, 12 Khuat Duy Tien, Thanh Xuan, Ha Noi</div>
        </div>
        <div class="item-contact-address">
            <div class="sub-title">Phone</div>
            <div class="text-contact-info">(+84).345.736.211</div>
        </div>
        <div class="item-contact-address">
            <div class="sub-title">Email</div>
            <div class="text-contact-info"><span>hotro@mediaiconic.com</span></div>
        </div>
        <ul>
            <li><a href=""><img src="/img/icon-facebook.png" alt="facebook"></a></li>
            <li><a href=""><img src="/img/icon-youtube.png" alt="youtube"></a></li>
            <li><a href=""><img src="/img/icon-tw.png" alt="tw"></a></li>
            <li><a href=""><img src="/img/icon-ig.png" alt="ig"></a></li>
        </ul>
    </div>
  </div>