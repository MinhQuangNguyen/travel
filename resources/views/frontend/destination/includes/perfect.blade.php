<div class="perfect perfect-destination">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-perfect">
                    <div class="subtitle text-center">Places to go</div>
                    <div class="title text-center">Perfect Destinations</div>
                    <div class="text text-center w-40-text">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        nisi ut aliquip
                        ex ea commodo consequat.</div>

                    <div class="row">
                        <div class="col-12 col-lg-4">
                            <div class="item-word-globo">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="title-main-list-word">
                                            Continents
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="location-map">Africa <span>(2)</span></div>
                                        <div class="location-map">Americas <span>(1)</span></div>
                                        <div class="location-map">Argentina <span>(2)</span></div>
                                        <div class="location-map">Asia <span>(5)</span></div>
                                    </div>
                                    <div class="col-6">
                                        <div class="location-map">Cambodia <span>(3)</span></div>
                                        <div class="location-map">Canada <span>(5)</span></div>
                                        <div class="location-map">Colombia <span>(2)</span></div>
                                        <div class="location-map">Costa Rica <span>(1)</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="item-word-globo">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="title-main-list-word">
                                            Asia & The Pacific
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="location-map">Africa <span>(2)</span></div>
                                        <div class="location-map">Americas <span>(1)</span></div>
                                        <div class="location-map">Argentina <span>(2)</span></div>
                                        <div class="location-map">Asia <span>(5)</span></div>
                                    </div>
                                    <div class="col-6">
                                        <div class="location-map">Cambodia <span>(3)</span></div>
                                        <div class="location-map">Canada <span>(5)</span></div>
                                        <div class="location-map">Colombia <span>(2)</span></div>
                                        <div class="location-map">Costa Rica <span>(1)</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="item-word-globo">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="title-main-list-word">
                                            Continents
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="location-map">Africa <span>(2)</span></div>
                                        <div class="location-map">Americas <span>(1)</span></div>
                                        <div class="location-map">Argentina <span>(2)</span></div>
                                        <div class="location-map">Asia <span>(5)</span></div>
                                    </div>
                                    <div class="col-6">
                                        <div class="location-map">Cambodia <span>(3)</span></div>
                                        <div class="location-map">Canada <span>(5)</span></div>
                                        <div class="location-map">Colombia <span>(2)</span></div>
                                        <div class="location-map">Costa Rica <span>(1)</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
