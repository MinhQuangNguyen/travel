<div class="destination">
    <div class="shape-choose"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="item-list-destination">
                <div class="item-destination">
                    <img src="img/destination1.png" alt="">
                    <div class="content">
                        <div class="title">Africa</div>
                        <div class="text">2 Tours</div>
                    </div>
                </div>
                <div class="item-destination">
                    <img src="img/destination2.png" alt="">
                    <div class="content">
                        <div class="title">Beaches</div>
                        <div class="text">10 Tours</div>
                    </div>
                </div>
                <div class="item-destination">
                    <img src="img/destination3.png" alt="">
                    <div class="content">
                        <div class="title">Japan</div>
                        <div class="text">4 Tours</div>
                    </div>
                </div>
                <div class="item-destination">
                    <img src="img/destination4.png" alt="">
                    <div class="content">
                        <div class="title">Iceland</div>
                        <div class="text">5 Tours</div>
                    </div>
                </div>
                <div class="item-destination">
                    <img src="img/destination5.png" alt="">
                    <div class="content">
                        <div class="title">Europe</div>
                        <div class="text">2 Tours</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
