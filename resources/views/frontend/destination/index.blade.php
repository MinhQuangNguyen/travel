@extends('frontend.layouts.alien')

@section('title', __('Destination'))

@section('content')
@include('frontend.destination.includes.headerSubPage')
@include('frontend.destination.includes.destinationContent')
@include('frontend.destination.includes.perfect')
@endsection
