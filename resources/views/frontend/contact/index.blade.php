@extends('frontend.layouts.alien')

@section('title', __('Contact'))

@section('content')
@include('frontend.contact.includes.headerSubPage')
@include('frontend.contact.includes.contact')
@include('frontend.contact.includes.map')
@endsection
