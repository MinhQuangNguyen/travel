<div class="main-contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="item-contact-address">
                    <div class="item-contact-main">
                        <img src="img/map-contact.svg" alt="">
                        <div class="info-content-contact-mqn">
                            <div class="title">Address</div>
                            <div class="text-contact">Zentowe Building, 12 Khuat Duy Tien
                                Thanh Xuan, Ha Noi</div>
                        </div>
                    </div>
                    <div class="item-contact-main">
                        <img src="img/phone-contact.svg" alt="">
                        <div class="info-content-contact-mqn">
                            <div class="title">+ 844 1800 - 333 55</div>
                            <div class="title">+ 84 345 736 211</div>
                        </div>
                    </div>
                    <div class="item-contact-main">
                        <img src="img/email-contact.svg" alt="">
                        <div class="info-content-contact-mqn">
                            <div class="title">Email</div>
                            <a href="mailto:hotro@mediaiconic.com">hotro@mediaiconic.com</a>
                        </div>
                    </div>
                    <div class="item-contact-main">
                        <img src="img/hour-contact.svg" alt="">
                        <div class="info-content-contact-mqn">
                            <div class="title">Business Hours</div>
                            <div class="text-contact">Mon- Sat: 10:00 AM - 20:00 PM</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-contact-main">
                    <div class="title">Send us a message</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
                    <form action="" method="POST">
                        <div class="form-group">
                            <input type="text" class="form-control"  placeholder="Your name *" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control"  placeholder="Your mail *" required>
                        </div>
                        <div class="form-group">
                          <textarea name=""  class="form-control" placeholder="Messager *" cols="30" rows="10" required></textarea>
                        </div>
                        <button type="submit" class="btn">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
