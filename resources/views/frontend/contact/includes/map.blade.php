<div class="contact-map">
    <div id="map" width="100%" style="height: 400px"></div>
</div>

@push('after-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgOuZWde1UFUOIunPgFFabrbsjPhKxTF0&callback=initMap&libraries=&v=weekly" defer></script>
<script>
    let map;
    function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng(20.9935323,105.8000186),
            zoom: 16,
        });
        const icons = {
            info: {
                icon: "{{asset('/img/icon-map-mqn.svg') }}",
            },
        };
        const features = [{
            position: new google.maps.LatLng(20.9935323,105.8000186),
            type: "info",
        }];

        // Create markers.
        for (let i = 0; i < features.length; i++) {
            const marker = new google.maps.Marker({
                position: features[i].position,
                icon: icons[features[i].type].icon,
                map: map,
            });
        }
    }
</script>
@endpush
