<div class="poputour">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-title-poputour">
                    <div class="sub-title">What’s new</div>
                    <div class="title">Popular Tours</div>
                </div>
            </div>
        </div>

        <div class="list-popular-tour">
            <div class="row">
                <div class="col-12 d-block d-lg-none">
                    <div class="item-tour">
                        <div class="content-header-tour">
                            <img src="img/tour1.png" alt="tour1" class="avata-tour">
                            <div class="item-tag-time">
                                <div class="row no-gutters">
                                    <div class="col-7 col-lg-6">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/calendar-solid.png" alt="calendar">
                                                    <div class="text">4 days</div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/user-solid.png" alt="user">
                                                    <div class="text">10</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 col-lg-6">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-info-tour">
                            <div class="title">Caño Cristales River Trip</div>
                            <div class="item-map">
                                <img src="img/map-icon.png" alt="">
                                <div class="location">Bryce Canyon National Park, USA</div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="price-tour">
                                        <div class="from">From</div>
                                        <div class="price">$80</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <a href="">
                                        <span>Explore Now</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-arrow-right color-orange"
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd"
                                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-8 d-none d-lg-block">
                    <div class="item-tour item-tour-pc">
                        <div class="content-header-tour">
                            <div class="row gutters">
                                <div class="col-6">
                                    <img src="img/tour1.png" alt="tour1" class="avata-tour-pc">
                                </div>
                                <div class="col-6">
                                    <div class="item-tag-time-pc">
                                        <div class="row no-gutters">
                                            <div class="col-7">
                                                <div class="row no-gutters">
                                                    <div class="col-5 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="img/calendar-solid.png" alt="calendar">
                                                            <div class="text">4 days</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-7 col-lg-6">
                                                        <div class="item-time">
                                                            <img src="img/user-solid.png" alt="user">
                                                            <div class="text">10</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                                <i class="fa fa-star green"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-info-tour">
                                        <div class="title">Caño Cristales River Trip</div>
                                        <div class="item-map">
                                            <img src="img/map-icon.png" alt="">
                                            <div class="location">Bryce Canyon National Park, USA</div>
                                        </div>
                                        <div class="text-tour">Caño Cristales Tour, (aka the most beautiful river in the world) is an exceptional and surprising natural beauty. Its unique ecosystem is very fragile and now belongs to the Sierra...</div>
                                       <div class="option-tour-mqn">
                                        <div class="row">
                                            <div class="col-3">
                                                <img src="img/option-tour1.svg" alt="option-tour" class="option-tour cus-option">
                                                <div class="text-option">Museum Tickets</div>
                                            </div>
                                            <div class="col-3">
                                                <img src="img/option-tour2.svg" alt="option-tour" class="option-tour">
                                                <div class="text-option">Sightseeing Tours</div>
                                            </div>
                                            <div class="col-3">
                                                <img src="img/option-tour3.svg" alt="option-tour" class="option-tour">
                                                <div class="text-option">Social Dinner</div>
                                            </div>
                                            <div class="col-3">
                                                <img src="img/option-tour4.svg" alt="option-tour" class="option-tour">
                                                <div class="text-option">Wine Tasting</div>
                                            </div>
                                        </div>
                                       </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="price-tour">
                                                    <div class="from">From</div>
                                                    <div class="price">$80</div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <a href="">
                                                    <span>Explore Now</span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                        fill="currentColor" class="bi bi-arrow-right color-orange"
                                                        viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                            d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="item-tour">
                        <div class="content-header-tour">
                            <img src="img/tour2.png" alt="tour1" class="avata-tour">
                            <div class="item-tag-time">
                                <div class="row no-gutters">
                                    <div class="col-7 col-lg-6">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/calendar-solid.png" alt="calendar">
                                                    <div class="text">4 days</div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/user-solid.png" alt="user">
                                                    <div class="text">10</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 col-lg-6">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-info-tour">
                            <div class="title">Caño Cristales River Trip</div>
                            <div class="item-map">
                                <img src="img/map-icon.png" alt="">
                                <div class="location">Bryce Canyon National Park, USA</div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="price-tour">
                                        <div class="from">From</div>
                                        <div class="price">$80</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <a href="">
                                        <span>Explore Now</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-arrow-right color-orange"
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd"
                                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="item-tour">
                        <div class="content-header-tour">
                            <img src="img/tour3.png" alt="tour1" class="avata-tour">
                            <div class="item-tag-time">
                                <div class="row no-gutters">
                                    <div class="col-7 col-lg-6">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/calendar-solid.png" alt="calendar">
                                                    <div class="text">4 days</div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/user-solid.png" alt="user">
                                                    <div class="text">10</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 col-lg-6">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-info-tour">
                            <div class="title">Caño Cristales River Trip</div>
                            <div class="item-map">
                                <img src="img/map-icon.png" alt="">
                                <div class="location">Bryce Canyon National Park, USA</div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="price-tour">
                                        <div class="from">From</div>
                                        <div class="price">$80</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <a href="">
                                        <span>Explore Now</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-arrow-right color-orange"
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd"
                                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="item-tour">
                        <div class="content-header-tour">
                            <img src="img/tour4.png" alt="tour1" class="avata-tour">
                            <div class="item-tag-time">
                                <div class="row no-gutters">
                                    <div class="col-7 col-lg-6">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/calendar-solid.png" alt="calendar">
                                                    <div class="text">4 days</div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/user-solid.png" alt="user">
                                                    <div class="text">10</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 col-lg-6">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-info-tour">
                            <div class="title">Caño Cristales River Trip</div>
                            <div class="item-map">
                                <img src="img/map-icon.png" alt="">
                                <div class="location">Bryce Canyon National Park, USA</div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="price-tour">
                                        <div class="from">From</div>
                                        <div class="price">$80</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <a href="">
                                        <span>Explore Now</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-arrow-right color-orange"
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd"
                                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="item-tour">
                        <div class="content-header-tour">
                            <img src="img/tour5.png" alt="tour1" class="avata-tour">
                            <div class="item-tag-time">
                                <div class="row no-gutters">
                                    <div class="col-7 col-lg-6">
                                        <div class="row no-gutters">
                                            <div class="col-7 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/calendar-solid.png" alt="calendar">
                                                    <div class="text">4 days</div>
                                                </div>
                                            </div>
                                            <div class="col-5 col-lg-6">
                                                <div class="item-time">
                                                    <img src="img/user-solid.png" alt="user">
                                                    <div class="text">10</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 col-lg-6">
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                        <i class="fa fa-star green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-info-tour">
                            <div class="title">Caño Cristales River Trip</div>
                            <div class="item-map">
                                <img src="img/map-icon.png" alt="">
                                <div class="location">Bryce Canyon National Park, USA</div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="price-tour">
                                        <div class="from">From</div>
                                        <div class="price">$80</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <a href="">
                                        <span>Explore Now</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-arrow-right color-orange"
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd"
                                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>