<div class="our-blog">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-header-blog">
                    <div class="sub-title">Testimonials</div>
                    <div class="title">Customer Reviews</div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <a href="">
                    <div class="item-blog">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <img src="img/tour9.png" alt="blog" class="avata-blog">
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="content-blog-mqn">
                                    <div class="tag">Company Insight</div>
                                        <div class="title-blog">Pack wisely before traveling</div>
                                        <div class="desc">A wonderful serenity has taken possession of my entire soul, like these sweet
                                            mornings of spring…</div>
                                        <div class="info-item-blog-mqn">
                                            <div class="item-info-blog">
                                                <img src="img/calendar-solid.png" alt="">
                                                <div class="text-info-blog">08/06/2021</div>
                                            </div>
                                            <div class="item-info-blog">
                                                <img src="img/user-solid.png" alt="">
                                                <div class="text-info-blog"><span>By</span> Admin</div>
                                            </div>
                                            <div class="item-info-blog">
                                                <img src="img/comment-solid.png" alt="">
                                                <div class="text-info-blog">20</div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-6">
                <a href="">
                    <div class="item-blog">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <img src="img/tour10.png" alt="blog" class="avata-blog">
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="content-blog-mqn">
                                    <div class="tag">Tips & Tricks</div>
                                    <div class="title-blog">Even the all-powerful Pointing</div>
                                    <div class="desc">A wonderful serenity has taken possession of my entire soul, like these sweet
                                        mornings of spring…</div>
                                    <div class="info-item-blog-mqn">
                                        <div class="item-info-blog">
                                            <img src="img/calendar-solid.png" alt="">
                                            <div class="text-info-blog">08/06/2021</div>
                                        </div>
                                        <div class="item-info-blog">
                                            <img src="img/user-solid.png" alt="">
                                            <div class="text-info-blog"><span>By</span> Admin</div>
                                        </div>
                                        <div class="item-info-blog">
                                            <img src="img/comment-solid.png" alt="">
                                            <div class="text-info-blog">20</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
