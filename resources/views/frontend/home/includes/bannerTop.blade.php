<div class="banner-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="title-section">
                    <div class="sub-title gs_reveal gs_reveal_fromLeft">Don’t miss</div>
                    <div class="title gs_reveal gs_reveal_fromRight">Special Offers</div>
                </div>
            </div>
        </div>
        <div class="content-service">
            <div class="row">
                <div class="col-12 col-lg-4 gs_reveal gs_reveal_fromLeft">
                    <div class="item-service" style="background: url(img/banner1.png) no-repeat">
                        <div class="title"> Weekly <br> Flash Deals</div>
                        <div class="text">Up to 30% off</div>
                        <a href="">
                            <span>Explore Now</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right color-blue" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="col-12 col-lg-4 gs_reveal gs_reveal_fromTop">
                    <div class="item-service" style="background: url(img/banner2.png) no-repeat">
                        <div class="title">Summer <br>Escapes</div>
                        <div class="text">Plan your next trip</div>
                        <a href="">
                            <span>Learn more</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right color-blue" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="col-12 col-lg-4 gs_reveal gs_reveal_fromRight">
                    <div class="item-service" style="background: url(img/banner3.png) no-repeat">
                        <div class="title">Exclusive <br> Deals</div>
                        <div class="text">Want to save up to 50%</div>
                        <a href="">
                            <span>Explore Now</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right color-blue" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>