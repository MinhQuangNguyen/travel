<div class="banner-search-option gs_reveal gs_reveal_fromTop">
    <div class="container-fluid">
        <div class="row">
            <div class="list-option-search">
                <div class="item-option-search">
                    <img src="/img/icon-map.png" alt="icon-map" class="img-icon w-24">
                    <div class="content">
                        <div class="title">Destinations</div>
                        <div class="text">Where are you going?</div>
                    </div>
                </div>
                <div class="item-option-search">
                    <img src="/img/activity.png" alt="icon-activity" class="img-icon">
                    <div class="content">
                        <div class="title">Activity</div>
                        <div class="text">All Activity</div>
                    </div>
                </div>
                <div class="item-option-search">
                    <img src="/img/Calendar.png" alt="icon-Calendar" class="img-icon ">
                    <div class="content">
                        <div class="title">When</div>
                        <div class="text">Date from</div>
                    </div>
                </div>
                <div class="item-option-search none-border">
                    <img src="/img/guest.png" alt="icon-guest" class="img-icon w-24">
                    <div class="content">
                        <div class="title">Guests</div>
                        <div class="text">0</div>
                    </div>
                </div>
                <div class="item-option-search">
                    <button type="button">
                        <img src="/img/search.png" alt="">
                        <span>Search</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>