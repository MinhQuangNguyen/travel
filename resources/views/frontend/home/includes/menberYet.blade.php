<div class="menber-yet">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-12 col-lg-6">
                <div class="content-menber-yet">
                    <img src="img/vector1.png" alt="" class="gs_reveal gs_reveal_fromLeft">
                    <div class="title d-block d-lg-none">Not a Member Yet?</div>
                    <div class="text d-block d-lg-none">Join us! Our members can access savings of up to 50% and earn
                        Trip Coins while
                        booking.</div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="button-menber gs_reveal gs_reveal_fromRight">
                    <div class="title d-none d-lg-block">Not a Member Yet?</div>
                    <div class="text d-none d-lg-block">Join us! Our members can access savings of up to 50% and earn
                        Trip Coins while
                        booking.</div>
                    <div class="button-login orange">
                        <a href="">
                            <span>Explore Now</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right color-white" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </a>
                    </div>

                    <div class="button-login blue">
                        <a href="">
                            <span>Explore Now</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right color-orange" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>