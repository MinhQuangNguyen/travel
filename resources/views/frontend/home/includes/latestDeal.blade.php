<div class="latest-deal">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-12 col-lg-6">
                <div class="img-header-latest"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="content-lastest-mqn">
                    <div class="sub-title">Latest deal</div>
                    <div class="title">Save an Extra $500 on Cyprus Holiday</div>
                    <div class="text">It’s limited seating! Hurry up</div>
                    <div class="row">
                        <div class="col-3">
                            <span id="days" class="text-number"></span>
                            <div class="text-desc">DAYS</div>
                        </div>
                        <div class="col-3">
                            <span id="hours" class="text-number"></span>
                            <div class="text-desc">HOURS</div>
                        </div>
                        <div class="col-3">
                            <span id="minutes" class="text-number"></span>
                            <div class="text-desc">MINS</div>
                        </div>
                        <div class="col-3">
                            <span id="seconds" class="text-number"></span>
                            <div class="text-desc">SECS</div>
                        </div>
                    </div>
                    <div class="item-border-bottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>