<div class="choose-us">
    <div class="shape-choose"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-why-choose-us">
                    <div class="sub-title">Brilliant reasons</div>
                    <div class="title">Why Choose Us</div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-choose-us">
                    <img src="img/vector2.png" alt="hinhanh">
                    <div class="title">Competitive Pricing</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-choose-us">
                    <img src="img/vector3.png" alt="hinhanh">
                    <div class="title">Worldwide Coverage</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-choose-us">
                    <img src="img/vector4.png" alt="hinhanh">
                    <div class="title">Fast Booking</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item-choose-us">
                    <img src="img/vector5.png" alt="hinhanh" class="mb-0">
                    <div class="title">Guided Tours</div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                </div>
            </div>
        </div>
    </div>
</div>