<div class="banner-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="content-header">
                    <div class="text gs_reveal gs_reveal_fromLeft">Natural beauty</div>
                    <h1 class="gs_reveal gs_reveal_fromRight">Discover the most engaging places</h1>
                    <a href="" class="gs_reveal gs_reveal_fromLeft">
                        <span>Explore Now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-right color-white" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
