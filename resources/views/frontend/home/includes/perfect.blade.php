<div class="perfect">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="content-perfect">
                    <div class="subtitle">Places to go</div>
                    <div class="title">Perfect Destinations</div>
                    <div class="text">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat.</div>
                    <div class="row">
                        <div class="col-6">
                            <div class="location-map">Africa <span>(2)</span></div>
                            <div class="location-map">Americas <span>(1)</span></div>
                            <div class="location-map">Argentina <span>(2)</span></div>
                            <div class="location-map">Asia <span>(5)</span></div>
                        </div>
                        <div class="col-6">
                            <div class="location-map">Cambodia <span>(3)</span></div>
                            <div class="location-map">Canada <span>(5)</span></div>
                            <div class="location-map">Colombia <span>(2)</span></div>
                            <div class="location-map">Costa Rica <span>(1)</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-9">
                <div class="slide-tour-perfect">
                    <div class="partner-home-slide-group">
                        <div class="partner-home-slide">
                            <div class="item">
                                <div class="item-tour-perfect">
                                    <img src="img/tour6.png" alt="">
                                    <div class="item-background"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="title-tour">Argentina</div>
                                        </div>
                                        <div class="col-6">
                                            <div class="number-tour">2 Tour</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item-tour-perfect">
                                    <img src="img/tour7.png" alt="">
                                    <div class="item-background"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="title-tour">Asia</div>
                                        </div>
                                        <div class="col-6">
                                            <div class="number-tour">4 Tour</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item-tour-perfect">
                                    <img src="img/tour8.png" alt="">
                                    <div class="item-background"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="title-tour">Combodia</div>
                                        </div>
                                        <div class="col-6">
                                            <div class="number-tour">6 Tour</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item-tour-perfect">
                                    <img src="img/tour9.png" alt="">
                                    <div class="item-background"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="title-tour">Paris</div>
                                        </div>
                                        <div class="col-6">
                                            <div class="number-tour">4 Tour</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-kc btn-back partner-prev">
                            <div class="btn-content"></div>
                        </div>
                        <div class="btn-kc btn-next partner-next">
                            <div class="btn-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
