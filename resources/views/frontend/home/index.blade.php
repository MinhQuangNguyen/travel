@extends('frontend.layouts.alien')

@section('title', __('Home'))

@section('content')
@include('frontend.home.includes.bannerHeader')
@include('frontend.home.includes.bannerSearchOption')
@include('frontend.home.includes.bannerTop')
@include('frontend.home.includes.menberYet')
@include('frontend.home.includes.poputour')
@include('frontend.home.includes.perfect')
@include('frontend.home.includes.chooseUs')
@include('frontend.home.includes.latestDeal')
@include('frontend.home.includes.customer')
@include('frontend.home.includes.ourBlog')
@endsection
