<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | 404</title>
    <meta name="author" content="@yield('meta_author', 'Minh Quang Nguyen')">

    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:description" name="description" content="Webiste Blog chia sẻ kiến thức thiết kế đồ họa và khóa học thiết kế đồ họa online miễn phí." />
    <meta property="og:site_name" content="{{ appURL() }}" />
    <meta property="og:image" content="/img/meta.png" />
    <link rel="icon" sizes="94x94" type="image/gif" href="/img/flaticon.png"/>
    @yield('meta')

    @stack('before-styles')
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')

</head>

<body>

    <div id="app">
        @include('frontend.includes.header', ['menu' => $menu])

        {{-- @include('includes.partials.messages') --}}

        <div class="error-main">
            <div class="container-fluid">
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-lg-6">
                        <img src="img/404.png" alt="404">
                        <div class="title">Oops! Look like you’re lost</div>
                        <div class="text">Page does not exist or some other error occured. Go to our <span>Home page</span></div>
                        <form action="" method="post">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon2" required>
                                <div class="input-group-append">
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('frontend.includes.footer')
    </div>
    <!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/frontend.js') }}"></script>
    <livewire:scripts />
    @include('frontend.includes.js')
    @stack('after-scripts')
    
</body>
</html>
