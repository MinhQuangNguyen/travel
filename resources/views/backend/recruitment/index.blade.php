@extends('backend.layouts.app')

@section('title', __('Recruitment Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Recruitment Management')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action"
                    :href="route('admin.recruitment.create')"
                    :text="__('Create Recruitment')"
                />
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.recruitments-table />
        </x-slot>
    </x-backend.card>
@endsection
