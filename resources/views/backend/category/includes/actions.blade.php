<x-utils.edit-button :href="route('admin.category.edit', $category)" />
<x-utils.delete-button :href="route('admin.category.destroy', $category)" />
