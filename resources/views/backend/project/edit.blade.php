@extends('backend.layouts.app')

@section('title', __('Update Project'))

@push('after-styles')
    <style>
        .card-header .fa {
            transition: .3s transform ease-in-out;
        }
        .card-header .collapsed .fa {
            transform: rotate(90deg);
        }
    </style>
@endpush('after-styles')

@section('content')
    <x-forms.patch :action="route('admin.project.update', $project)" enctype="multipart/form-data">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Project')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.project.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div x-data="{categoryId: '{{ $project->category_id }}'}">
                    @include('backend.project.includes.info-project', $project)
                    @include('backend.project.includes.overview-project', $project)
                    @include('backend.project.includes.position-project', $project)
                    @include('backend.project.includes.utilities-project', $project)
                    @include('backend.project.includes.images-project', $project)
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>

    @push('after-scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("select").select2({
                placeholder: "{{ __('Select a option') }}",
                allowClear: true
            });

            var previewImages = function(input, imgPreviewPlaceholder, attSrc) {
                if (input.files) {
                    var filesAmount = input.files.length;
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            $(attSrc).attr('src', event.target.result);
                        }
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            };
            $('#images').on('change', function() {
                previewImages(this, 'div.images-preview-div', '#image-pre');
            });

            $('#image-position').on('change', function() {
                previewImages(this, 'div.images-preview-position', '#image-position-pre');
            });
        })
    </script>
    @endpush('after-scripts')
@endsection
